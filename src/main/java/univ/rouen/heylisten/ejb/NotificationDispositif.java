package univ.rouen.heylisten.ejb;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class NotificationDispositif implements Serializable {

    @Id
    @GeneratedValue
    private long idNotifDispositif;

    @ManyToOne
    @JoinColumn(name = "idDispositif")
    private DispositifMedical dispositif;

    @ManyToOne
    @JoinColumn(name = "idUtilisateur")
    private Utilisateur utilisateur;

    @ManyToOne
    @JoinColumn(name = "idEffetIndesirable")
    private EffetIndesirable effetIndesirable;

    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date dateNotif;

    public NotificationDispositif() {}

    public long getIdNotifDispositif() {
        return idNotifDispositif;
    }

    public void setIdNotifDispositif(long idNotifDispositif) {
        this.idNotifDispositif = idNotifDispositif;
    }

    public DispositifMedical getDispositif() {
        return dispositif;
    }

    public void setDispositif(DispositifMedical dispositif) {
        this.dispositif = dispositif;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Date getDateNotif() {
        return dateNotif;
    }

    public void setDateNotif(Date dateNotif) {
        this.dateNotif = dateNotif;
    }

    public EffetIndesirable getEffetIndesirable() {
        return effetIndesirable;
    }

    public void setEffetIndesirable(EffetIndesirable effetIndesirable) {
        this.effetIndesirable = effetIndesirable;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NotificationDispositif{");
        sb.append("idNotifDispositif=").append(idNotifDispositif);
        sb.append(", dispositif=").append(dispositif);
        sb.append(", utilisateur=").append(utilisateur);
        sb.append(", dateNotif=").append(dateNotif);
        sb.append('}');
        return sb.toString();
    }
}

package univ.rouen.heylisten.ejb;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class CasSubstanceEI implements Serializable{
    @Id
    @GeneratedValue
    private long idCasSubstanceEI;

    @ManyToOne
    private SubstanceActive substance;

    @ManyToOne
    private EffetIndesirable effetIndesirableSubstance;

    public CasSubstanceEI() {}

    public long getIdCasSubstanceEI() {
        return idCasSubstanceEI;
    }

    public void setIdCasSubstanceEI(long idCasSubstanceEI) {
        this.idCasSubstanceEI = idCasSubstanceEI;
    }

    public SubstanceActive getSubstance() {
        return substance;
    }

    public void setSubstance(SubstanceActive substance) {
        this.substance = substance;
    }

    public EffetIndesirable getEffetIndesirableSubstance() {
        return effetIndesirableSubstance;
    }

    public void setEffetIndesirableSubstance(EffetIndesirable effetIndesirableSubstance) {
        this.effetIndesirableSubstance = effetIndesirableSubstance;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CasSubstanceEI{");
        sb.append("idCasSubstanceEI=").append(idCasSubstanceEI);
        sb.append(", substance=").append(substance);
        sb.append(", effetIndesirableSubstance=").append(effetIndesirableSubstance);
        sb.append('}');
        return sb.toString();
    }
}

package univ.rouen.heylisten.ejb;


import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Indexed
@AnalyzerDef(name="ingredientAnalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = NGramFilterFactory.class, params = {
                        @Parameter( name ="minGramSize",value= "1" ),
                        @Parameter( name="maxGramSize", value="15" )
                })
        })
public class Ingredient implements Serializable{
    @Id
    @GeneratedValue
    private long idIngredient;

    @NotNull
    @Size(min = 3, max = 150)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "ingredientAnalyzer")
    private String libelle;

    @ManyToMany(mappedBy = "ingredients")
    private Collection<ProduitCosmetique> produits;

    public Ingredient() {}

    public Ingredient( String libelle) { this.libelle = libelle; }

    public long getIdIngredient() { return idIngredient; }

    public void setIdIngredient(long idIngredient) { this.idIngredient = idIngredient; }

    public String getLibelle() { return libelle; }

    public void setLibelle(String libelle) { this.libelle = libelle; }

    public Collection<ProduitCosmetique> getProduits() {
        return produits;
    }

    public void setProduits(Collection<ProduitCosmetique> produits) {
        this.produits = produits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ingredient that = (Ingredient) o;

        if (idIngredient != that.idIngredient) return false;
        if (!libelle.equals(that.libelle)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idIngredient ^ (idIngredient >>> 32));
        result = 31 * result + libelle.hashCode();
        return result;
    }
}

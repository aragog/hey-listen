package univ.rouen.heylisten.ejb;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Role implements Serializable {
    public enum Roles {
        USER("ROLE_USER"), ADMIN("ROLE_ADMIN");

        private final String name;

        Roles(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }
    @Id
    @GeneratedValue
    private long id;

    private String role;

    public Role() {
    }

    public Role(String role) {
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Role{");
        sb.append("id=").append(id);
        sb.append(", role='").append(role).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

package univ.rouen.heylisten.ejb;

import javax.persistence.*;

@Entity
public class CasClasseChimiqueEI {
    @Id
    @GeneratedValue
    private long idCasClasseChimiqueEI;

    @ManyToOne
    @JoinColumn(name = "idClasseChimique")
    private ClasseChimique classeChimique;

    @ManyToOne
    @JoinColumn(name = "idEffetIndesirable")
    private EffetIndesirable effetIndesirableClasseChimique;

    public CasClasseChimiqueEI() {}

    public CasClasseChimiqueEI(ClasseChimique classeChimique, EffetIndesirable effetIndesirableClasseChimique) {
        this.classeChimique = classeChimique;
        this.effetIndesirableClasseChimique = effetIndesirableClasseChimique;
    }

    public EffetIndesirable getEffetIndesirableClasseChimique() {
        return effetIndesirableClasseChimique;
    }

    public void setEffetIndesirableClasseChimique(EffetIndesirable effetIndesirableClasseChimique) {
        this.effetIndesirableClasseChimique = effetIndesirableClasseChimique;
    }

    public ClasseChimique getClasseChimique() {
        return classeChimique;
    }

    public void setClasseChimique(ClasseChimique classeChimique) {
        this.classeChimique = classeChimique;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CasClasseChimiqueEI{");
        sb.append("idCasClasseChimiqueEI=").append(idCasClasseChimiqueEI);
        sb.append(", classeChimique=").append(classeChimique);
        sb.append(", effetIndesirableClasseChimique=").append(effetIndesirableClasseChimique);
        sb.append('}');
        return sb.toString();
    }
}

package univ.rouen.heylisten.ejb;


import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;


@Entity
@Indexed
@AnalyzerDef(name="classePharmacologiqueAnalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = NGramFilterFactory.class, params = {
                        @Parameter( name ="minGramSize",value= "1" ),
                        @Parameter( name="maxGramSize", value="15" )
                })
        })
public class ClassePharmacologique implements Serializable {
    @Id
    @GeneratedValue
    private long id;

    @NotNull
    @Size(min = 1, max = 3)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "classePharmacologiqueAnalyzer")
    private String idClassePharmacologique;

    @NotNull
    @Size(min = 1, max = 150)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "classePharmacologiqueAnalyzer")
    private String libelle;

    @ManyToMany
    @JoinTable(
            name = "ClassePharmacologique_SubstanceActive",
            joinColumns = {@JoinColumn(name = "id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "idSubstance", referencedColumnName = "idSubstance")}
    )
    private Collection<SubstanceActive> classePharmacologiqueSubstanceActives;

    @ManyToOne
    private ClassePharmacologique classePharmacologiquePere;

    @OneToMany(mappedBy = "classePharmacologiquePere")
    private Collection<ClassePharmacologique> classePharmacologiqueFils;

    @OneToMany(mappedBy = "classePharmacologique")
    private Collection<CasClassePharmacologiqueEI> casClassePharmacologiqueEI;

    public ClassePharmacologique() {}

    public ClassePharmacologique(String idClassePharmacologique, String libelle){
        this.idClassePharmacologique = idClassePharmacologique;
        this.libelle = libelle;
    }

    public ClassePharmacologique(String idClassePharmacologique, String libelle, ClassePharmacologique classePharmacologiquePere){
        this.idClassePharmacologique = idClassePharmacologique;
        this.libelle = libelle;
        this.classePharmacologiquePere = classePharmacologiquePere;
    }

    public ClassePharmacologique(String idClassePharmacologique, String libelle, Collection<SubstanceActive> classePharmacologiqueSubstanceActive,
                                 ClassePharmacologique classePharmacologiquePere, Collection<ClassePharmacologique> classePharmacologiqueFils) {
        this.idClassePharmacologique = idClassePharmacologique;
        this.libelle = libelle;
        this.classePharmacologiqueSubstanceActives = classePharmacologiqueSubstanceActive;
        this.classePharmacologiquePere = classePharmacologiquePere;
        this.classePharmacologiqueFils = classePharmacologiqueFils;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdClassePharmacologique() {
        return idClassePharmacologique;
    }

    public void setIdClassePharmacologique(String idClassePharmacologique) {
        this.idClassePharmacologique = idClassePharmacologique;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public ClassePharmacologique getClassePharmacologiquePere() {
        return classePharmacologiquePere;
    }

    public void setClassePharmacologiquePere(ClassePharmacologique classePharmacologiquePere) {
        this.classePharmacologiquePere = classePharmacologiquePere;
    }

    public Collection<ClassePharmacologique> getClassePharmacologiqueFils() {
        return classePharmacologiqueFils;
    }

    public void setClassePharmacologiqueFils(Collection<ClassePharmacologique> classePharmacologiqueFils) {
        this.classePharmacologiqueFils = classePharmacologiqueFils;
    }

    public Collection<SubstanceActive> getClassePharmacologiqueSubstanceActives() {
        return classePharmacologiqueSubstanceActives;
    }

    public void setClassePharmacologiqueSubstanceActives(Collection<SubstanceActive> classePharmacologiqueSubstanceActives) {
        this.classePharmacologiqueSubstanceActives = classePharmacologiqueSubstanceActives;
    }

    public Collection<CasClassePharmacologiqueEI> getCasClassePharmacologiqueEI() {
        return casClassePharmacologiqueEI;
    }

    public void setCasClassePharmacologiqueEI(Collection<CasClassePharmacologiqueEI> casClassePharmacologiqueEI) {
        this.casClassePharmacologiqueEI = casClassePharmacologiqueEI;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ClassePharmacologique{");
        sb.append("idClassePharmacologique='").append(idClassePharmacologique).append('\'');
        sb.append(", libelle='").append(libelle).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClassePharmacologique that = (ClassePharmacologique) o;

        if (id != that.id) return false;
        if (idClassePharmacologique != that.idClassePharmacologique) return false;
        if (!libelle.equals(that.libelle)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + idClassePharmacologique.hashCode() + libelle.hashCode();
        return result;
    }
}

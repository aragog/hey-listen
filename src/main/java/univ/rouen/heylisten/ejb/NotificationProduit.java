package univ.rouen.heylisten.ejb;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class NotificationProduit implements Serializable {

    @Id
    @GeneratedValue
    private long idNotifProduit;

    @ManyToOne
    @JoinColumn(name = "idProduit")
    private ProduitCosmetique produit;

    @ManyToOne
    @JoinColumn(name = "idUtilisateur")
    private Utilisateur utilisateur;

    @ManyToOne
    @JoinColumn(name = "idEffetIndesirable")
    private EffetIndesirable effetIndesirable;

    private Date dateNotif;

    public NotificationProduit() {}

    public ProduitCosmetique getProduit() {
        return produit;
    }

    public void setProduit(ProduitCosmetique produit) {
        this.produit = produit;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public long getIdNotifProduit() {
        return idNotifProduit;
    }

    public void setIdNotifProduit(long idNotifProduit) {
        this.idNotifProduit = idNotifProduit;
    }

    public Date getDateNotif() {
        return dateNotif;
    }

    public void setDateNotif(Date dateNotif) {
        this.dateNotif = dateNotif;
    }

    public EffetIndesirable getEffetIndesirable() {
        return effetIndesirable;
    }

    public void setEffetIndesirable(EffetIndesirable effetIndesirable) {
        this.effetIndesirable = effetIndesirable;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NotificationProduit{");
        sb.append("idNotifProduit=").append(idNotifProduit);
        sb.append(", produit=").append(produit);
        sb.append(", utilisateur=").append(utilisateur);
        sb.append(", dateNotif=").append(dateNotif);
        sb.append('}');
        return sb.toString();
    }
}
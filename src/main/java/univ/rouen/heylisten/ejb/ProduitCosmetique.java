package univ.rouen.heylisten.ejb;


import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Indexed
@AnalyzerDef(name="produitAnalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = NGramFilterFactory.class, params = {
                        @Parameter( name ="minGramSize",value= "1" ),
                        @Parameter( name="maxGramSize", value="15" )
                })
        })
public class ProduitCosmetique implements Serializable {

    @Id
    @GeneratedValue
    private long idProduit;

    @NotNull
    @Size(min = 3, max = 150)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "produitAnalyzer")
    private String libelle;

    @NotNull
    @ManyToOne
    private LaboratoireCosmetique labCosm;

    @ManyToMany
    @JoinTable(
            name = "Produit_Ingredient",
            joinColumns = {@JoinColumn(name = "idProduit", referencedColumnName = "idProduit")},
            inverseJoinColumns = {@JoinColumn(name = "idIngredient", referencedColumnName = "idIngredient")}
    )
    private Collection<Ingredient> ingredients;

    @OneToMany(mappedBy = "produit")
    private Collection<NotificationProduit>  notificationProduits;

    public ProduitCosmetique() {}

    public ProduitCosmetique(String libelle){ this.libelle = libelle; }

    public long getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(long idProduit) {
        this.idProduit = idProduit;
    }

    public LaboratoireCosmetique getLabCosm() {
        return labCosm;
    }

    public void setLabCosm(LaboratoireCosmetique labCosm) { this.labCosm = labCosm; }

    public String getLibelle() { return libelle; }

    public void setLibelle(String libelle) { this.libelle = libelle; }

    public Collection<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Collection<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public Collection<NotificationProduit> getNotificationProduits() {
        return notificationProduits;
    }

    public void setNotificationProduits(Collection<NotificationProduit> notificationProduits) {
        this.notificationProduits = notificationProduits;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProduitCosmetique{");
        sb.append("idProduit=").append(idProduit);
        sb.append(", libelle='").append(libelle).append('\'');
        sb.append(", labCosm=").append(labCosm);
        sb.append(", ingredients=").append(ingredients);
        sb.append(", notificationProduits=").append(notificationProduits);
        sb.append('}');
        return sb.toString();
    }
}
package univ.rouen.heylisten.ejb;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import univ.rouen.heylisten.util.UserType;

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Indexed
@AnalyzerDef(name="userAnalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = NGramFilterFactory.class, params = {
                        @Parameter( name ="minGramSize",value= "1" ),
                        @Parameter( name="maxGramSize", value="15" )
                })
        })
public class Utilisateur implements Serializable {
    @Id
    @GeneratedValue
    private long idUtilisateur;

    @NotNull
    private UserType type;

    @NotNull
    @Size(min = 3, max = 30)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "userAnalyzer")
    private String username;

    @NotNull
    @Size(min = 8, max = 100)
    private String passwd;

    @Column(columnDefinition = "TINYINT DEFAULT 1")
    private boolean enabled;

    @NotNull
    @ManyToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    private Region region;

    @OneToMany(mappedBy = "utilisateur")
    private Collection<NotificationMedicament> notificationMedicaments;

    @OneToMany(mappedBy = "utilisateur")
    private Collection<NotificationDispositif> notificationDispositifs;

    @OneToMany(mappedBy = "utilisateur")
    private Collection<NotificationProduit> notificationProduits;

    @ManyToMany
    @Cascade(CascadeType.SAVE_UPDATE)
    private Collection<Role> roles;

    public Utilisateur() {}

    public Utilisateur(UserType type, String username, String passwd, boolean enabled, Region region) {
        this.type = type;
        this.username = username;
        this.passwd = passwd;
        this.enabled = enabled;
        this.region = region;
    }

    public long getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(long idUtilisateur) { this.idUtilisateur = idUtilisateur; }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswd() {
        return passwd;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Collection<NotificationMedicament> getNotificationMedicaments() {
        return notificationMedicaments;
    }

    public void setNotificationMedicaments(Collection<NotificationMedicament> notificationMedicaments) {
        this.notificationMedicaments = notificationMedicaments;
    }

    public Collection<NotificationDispositif> getNotificationDispositifs() {
        return notificationDispositifs;
    }

    public void setNotificationDispositifs(Collection<NotificationDispositif> notificationDispositifs) {
        this.notificationDispositifs = notificationDispositifs;
    }

    public Collection<NotificationProduit> getNotificationProduits() {
        return notificationProduits;
    }

    public void setNotificationProduits(Collection<NotificationProduit> notificationProduits) {
        this.notificationProduits = notificationProduits;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Utilisateur{");
        sb.append("idUtilisateur=").append(idUtilisateur);
        sb.append(", type=").append(type);
        sb.append(", username='").append(username).append('\'');
        sb.append(", passwd='").append(passwd).append('\'');
        sb.append(", enabled=").append(enabled);
        sb.append(", region=").append(region);
        sb.append(", notificationMedicaments=").append(notificationMedicaments);
        sb.append(", notificationDispositifs=").append(notificationDispositifs);
        sb.append(", notificationProduits=").append(notificationProduits);
        sb.append(", roles=").append(roles);
        sb.append('}');
        return sb.toString();
    }
}

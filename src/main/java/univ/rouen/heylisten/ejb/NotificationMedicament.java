package univ.rouen.heylisten.ejb;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class NotificationMedicament implements Serializable {

    @Id
    @GeneratedValue
    private long idNotifMedicament;

    @ManyToOne
    @JoinColumn(name = "idMedicament")
    private Medicament medicament;

    @ManyToOne
    @JoinColumn(name = "idUtilisateur")
    private Utilisateur utilisateur;

    @ManyToOne
    @JoinColumn(name = "idEffetIndesirable")
    private EffetIndesirable effetIndesirable;

    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date dateNotif;

    public NotificationMedicament() {}

    public long getIdNotifMedicament() {
        return idNotifMedicament;
    }

    public void setIdNotifMedicament(long idNotifMedicament) {
        this.idNotifMedicament = idNotifMedicament;
    }

    public Medicament getMedicament() {
        return medicament;
    }

    public void setMedicament(Medicament medicament) {
        this.medicament = medicament;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Date getDateNotif() {
        return dateNotif;
    }

    public void setDateNotif(Date dateNotif) {
        this.dateNotif = dateNotif;
    }

    public EffetIndesirable getEffetIndesirable() {
        return effetIndesirable;
    }

    public void setEffetIndesirable(EffetIndesirable effetIndesirable) {
        this.effetIndesirable = effetIndesirable;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NotificationMedicament{");
        sb.append("idNotifMedicament=").append(idNotifMedicament);
        sb.append(", medicament=").append(medicament);
        sb.append(", utilisateur=").append(utilisateur);
        sb.append(", dateNotif=").append(dateNotif);
        sb.append('}');
        return sb.toString();
    }
}

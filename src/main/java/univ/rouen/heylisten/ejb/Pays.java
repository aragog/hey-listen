package univ.rouen.heylisten.ejb;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Pays implements Serializable {

    @Id
    @GeneratedValue
    private long idPays;

    private String libelle;

    @OneToMany(mappedBy = "pays")
    private Collection<Region> regions;

    public Pays() {}

    public Pays(String libelle) {this.libelle = libelle; }

    public long getIdPays() {
        return idPays;
    }

    public void setIdPays(long idPays) {
        this.idPays = idPays;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Collection<Region> getRegions() {
        return regions;
    }

    public void setRegions(Collection<Region> regions) {
        this.regions = regions;
    }

    @Override
    public String toString() {
        return this.libelle;
    }
}

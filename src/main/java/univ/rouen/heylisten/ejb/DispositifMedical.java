package univ.rouen.heylisten.ejb;


import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;


@Entity
@Indexed
@AnalyzerDef(name="dispositifAnalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = NGramFilterFactory.class, params = {
                        @Parameter( name ="minGramSize",value= "1" ),
                        @Parameter( name="maxGramSize", value="15" )
                })
        })
public class DispositifMedical implements Serializable{

    @Id
    @GeneratedValue
    private long idDispositif;

    @NotNull
    @ManyToOne
    private LaboratoirePharmacologique labPharm;

    @NotNull
    @Size(min = 2, max = 150)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "dispositifAnalyzer")
    private String libelle;

    @OneToMany(mappedBy = "dispositif")
    private Collection<NotificationDispositif> notificationDispositifs;

    public DispositifMedical(){}

    public DispositifMedical(String libelle){
        this.libelle = libelle;
    }

    public long getIdDispositif() { return idDispositif; }

    public void setIdDispositif(long idDispositif) { this.idDispositif = idDispositif; }

    public LaboratoirePharmacologique getLabPharm() { return labPharm; }

    public void setLabPharm(LaboratoirePharmacologique labPharm) { this.labPharm = labPharm; }

    public String getLibelle() { return libelle; }

    public void setLibelle(String libelle) { this.libelle = libelle; }

    public Collection<NotificationDispositif> getNotificationDispositifs() {
        return notificationDispositifs;
    }

    public void setNotificationDispositifs(Collection<NotificationDispositif> notificationDispositifs) {
        this.notificationDispositifs = notificationDispositifs;
    }

    @Override
    public String toString() {
        return this.libelle;
    }
}

package univ.rouen.heylisten.ejb;


import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Indexed
@AnalyzerDef(name="medocAnalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = NGramFilterFactory.class, params = {
                        @Parameter( name ="minGramSize",value= "1" ),
                        @Parameter( name="maxGramSize", value="15" )
                })
        })
public class Medicament implements Serializable {
    @Id
    @GeneratedValue
    private long idMedicament;

    @NotNull
    @Size(min = 8, max = 8)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "medocAnalyzer")
    private String code_cis;

    @NotNull
    @Size(min = 1, max = 150)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "medocAnalyzer")
    private String libelle;

    @OneToMany(mappedBy = "medicament")
    private Collection<CompositionMedicament> compositionMedicaments;

    @OneToMany(mappedBy = "medicament")
    private Collection<NotificationMedicament> notificationMedicaments;

    public Medicament() {
    }

    public Medicament(String code_cis, String libelle) {
        this.code_cis = code_cis;
        this.libelle = libelle;
    }

    public long getIdMedicament() {
        return idMedicament;
    }

    public void setIdMedicament(long idMedicament) {
        this.idMedicament = idMedicament;
    }

    public String getCode_cis() {
        return code_cis;
    }

    public void setCode_cis(String code_cis) {
        this.code_cis = code_cis;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Collection<CompositionMedicament> getCompositionMedicaments() {
        return compositionMedicaments;
    }

    public void setCompositionMedicaments(Collection<CompositionMedicament> compositionMedicaments) {
        this.compositionMedicaments = compositionMedicaments;
    }

    public Collection<NotificationMedicament> getNotificationMedicaments() {
        return notificationMedicaments;
    }

    public void setNotificationMedicaments(Collection<NotificationMedicament> notificationMedicaments) {
        this.notificationMedicaments = notificationMedicaments;
    }

    @Override
    public String toString() {
        return this.libelle;
    }
}

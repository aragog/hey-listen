package univ.rouen.heylisten.ejb;


import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Indexed
@AnalyzerDef(name="substanceAnalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = NGramFilterFactory.class, params = {
                        @Parameter( name ="minGramSize",value= "1" ),
                        @Parameter( name="maxGramSize", value="15" )
                })
        })
public class SubstanceActive implements Serializable {
    @Id
    private long idSubstance;

    @NotNull
    @Size(min = 3, max = 150)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "substanceAnalyzer")
    private String libelle;

    @ManyToMany(mappedBy = "classePharmacologiqueSubstanceActives")
    private Collection<ClassePharmacologique> classePharmacologiques;

    @ManyToMany(mappedBy = "classeChimiqueSubstanceActives")
    private Collection<ClasseChimique> classeChimiques;

    @OneToMany(mappedBy = "substanceActive")
    private Collection<CompositionMedicament> compositionMedicaments;

    @OneToMany(mappedBy = "substance")
    private Collection<CasSubstanceEI> casSubstanceEIs;

    public SubstanceActive() {}

    public SubstanceActive(long idSubstance, String libelle, Collection<ClassePharmacologique> classePharmacologiques, Collection<ClasseChimique> classeChimiques) {
        this.idSubstance = idSubstance;
        this.libelle = libelle;
        this.classePharmacologiques = classePharmacologiques;
        this.classeChimiques = classeChimiques;
    }

    public long getIdSubstance() {
        return idSubstance;
    }

    public void setIdSubstance(long idSubstance) {
        this.idSubstance = idSubstance;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Collection<ClassePharmacologique> getClassePharmacologiques() {
        return classePharmacologiques;
    }

    public void setClassePharmacologiques(Collection<ClassePharmacologique> classePharmacologiques) {
        this.classePharmacologiques = classePharmacologiques;
    }

    public Collection<ClasseChimique> getClasseChimiques() {
        return classeChimiques;
    }

    public void setClasseChimiques(Collection<ClasseChimique> classeChimiques) {
        this.classeChimiques = classeChimiques;
    }

    public Collection<CompositionMedicament> getCompositionMedicaments() {
        return compositionMedicaments;
    }

    public void setCompositionMedicaments(Collection<CompositionMedicament> compositionMedicaments) {
        this.compositionMedicaments = compositionMedicaments;
    }

    public Collection<CasSubstanceEI> getCasSubstanceEIs() {
        return casSubstanceEIs;
    }

    public void setCasSubstanceEIs(Collection<CasSubstanceEI> casSubstanceEIs) {
        this.casSubstanceEIs = casSubstanceEIs;
    }

    @Override
    public String toString() {
        return this.libelle;
    }
}

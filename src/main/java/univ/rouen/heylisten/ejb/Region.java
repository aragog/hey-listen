package univ.rouen.heylisten.ejb;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Region implements Serializable {

    @Id
    @GeneratedValue
    private long idRegion;

    private String libelle;

    @ManyToOne
    private Pays pays;

    @OneToMany(mappedBy = "region")
    private Collection<Utilisateur> utilisateurs;

    public Region() { }

    public Region(String libelle) { this.libelle = libelle; }

    public long getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(long idRegion) {
        this.idRegion = idRegion;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    public Collection<Utilisateur> getUtilisateurs() {
        return utilisateurs;
    }

    public void setUtilisateurs(Collection<Utilisateur> utilisateurs) {
        this.utilisateurs = utilisateurs;
    }

    @Override
    public String toString() {
        return this.libelle;
    }
}

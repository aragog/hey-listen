package univ.rouen.heylisten.ejb;


import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Indexed
@AnalyzerDef(name="customAnalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = NGramFilterFactory.class, params = {
                        @Parameter( name ="minGramSize",value= "1" ),
                        @Parameter( name="maxGramSize", value="15" )
                })
        })
public class ClasseChimique implements Serializable {
    @Id
    @GeneratedValue
    private long id;

    @NotNull
    @Size(min = 1, max = 3)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "customAnalyzer")
    private String idClasseChimique;

    @NotNull
    @Size(min = 1, max = 150)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "customAnalyzer")
    private String libelle;

    @ManyToMany
    @JoinTable(
            name = "ClasseChimique_SubstanceActive",
            joinColumns = {@JoinColumn(name = "id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "idSubstance", referencedColumnName = "idSubstance")}
    )
    private Collection<SubstanceActive> classeChimiqueSubstanceActives;

    @ManyToOne
    private ClasseChimique classeChimiquePere;

    @OneToMany(mappedBy = "classeChimiquePere")
    private Collection<ClasseChimique> classeChimiqueFils;

    @OneToMany(mappedBy = "classeChimique")
    private Collection<CasClasseChimiqueEI> casClasseChimiqueEI;

    public ClasseChimique() {}

    public ClasseChimique(String idClasseChimique, String libelle){
        this.idClasseChimique = idClasseChimique;
        this.libelle = libelle;
    }

    public ClasseChimique(String idClasseChimique, String libelle, ClasseChimique classeChimiquePere){
        this.idClasseChimique = idClasseChimique;
        this.libelle = libelle;
        this.classeChimiquePere = classeChimiquePere;
    }

    public ClasseChimique(String idClasseChimique, String libelle, Collection<SubstanceActive> classeChimiqueSubstanceActives,
                          ClasseChimique classeChimiquePere, Collection<ClasseChimique> classeChimiqueFils) {
        this.idClasseChimique = idClasseChimique;
        this.libelle = libelle;
        this.classeChimiqueSubstanceActives = classeChimiqueSubstanceActives;
        this.classeChimiquePere = classeChimiquePere;
        this.classeChimiqueFils = classeChimiqueFils;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdClasseChimique() {
        return idClasseChimique;
    }

    public void setIdClasseChimique(String idClasseChimique) {
        this.idClasseChimique = idClasseChimique;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public ClasseChimique getClasseChimiquePere() {
        return classeChimiquePere;
    }

    public void setClasseChimiquePere(ClasseChimique classeChimiquePere) {
        this.classeChimiquePere = classeChimiquePere;
    }

    public Collection<ClasseChimique> getClasseChimiqueFils() {
        return classeChimiqueFils;
    }

    public void setClasseChimiqueFils(Collection<ClasseChimique> classeChimiqueFils) {
        this.classeChimiqueFils = classeChimiqueFils;
    }

    public Collection<SubstanceActive> getClasseChimiqueSubstanceActives() {
        return classeChimiqueSubstanceActives;
    }

    public void setClasseChimiqueSubstanceActives(Collection<SubstanceActive> classeChimiqueSubstanceActives) {
        this.classeChimiqueSubstanceActives = classeChimiqueSubstanceActives;
    }

    public Collection<CasClasseChimiqueEI> getCasClasseChimiqueEI() {
        return casClasseChimiqueEI;
    }

    public void setCasClasseChimiqueEI(Collection<CasClasseChimiqueEI> casClasseChimiqueEI) {
        this.casClasseChimiqueEI = casClasseChimiqueEI;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ClasseChimique{");
        sb.append("idClasseChimique='").append(idClasseChimique).append('\'');
        sb.append(", libelle='").append(libelle).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClasseChimique that = (ClasseChimique) o;

        if (id != that.id) return false;
        if (idClasseChimique != that.idClasseChimique) return false;
        if (!libelle.equals(that.libelle)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + idClasseChimique.hashCode() + libelle.hashCode();
        return result;
    }
}

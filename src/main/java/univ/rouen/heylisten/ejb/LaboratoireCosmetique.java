package univ.rouen.heylisten.ejb;


import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;


@Entity
@Indexed
@AnalyzerDef(name="labCosmAnalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = NGramFilterFactory.class, params = {
                        @Parameter( name ="minGramSize",value= "1" ),
                        @Parameter( name="maxGramSize", value="15" )
                })
        })
public class LaboratoireCosmetique implements Serializable {

    @Id
    @GeneratedValue
    private long idLabCosm;

    @NotNull
    @Size(min = 3, max = 150)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "labCosmAnalyzer")
    private String libelle;

    @OneToMany(mappedBy = "labCosm")
    private Collection<ProduitCosmetique> produits;

    public LaboratoireCosmetique(){}

    public LaboratoireCosmetique(String libelle){ this.libelle = libelle; }

    public long getIdLabCosm() { return idLabCosm; }

    public void setIdLabCosm(long idLabCosm) { this.idLabCosm = idLabCosm; }

    public String getLibelle() { return libelle; }

    public void setLibelle(String libelle) { this.libelle = libelle; }

    public Collection<ProduitCosmetique> getProduits() { return produits; }

    public void setProduits(Collection<ProduitCosmetique> produits) { this.produits = produits; }
}



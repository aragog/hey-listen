package univ.rouen.heylisten.ejb;


import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;


@Entity
@Indexed
@AnalyzerDef(name="labPharmAnalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = NGramFilterFactory.class, params = {
                        @Parameter( name ="minGramSize",value= "1" ),
                        @Parameter( name="maxGramSize", value="15" )
                })
        })
public class LaboratoirePharmacologique implements Serializable {

    @Id
    @GeneratedValue
    private long idLabPharm;

    @NotNull
    @Size(min = 3, max = 150)
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Analyzer(definition = "labPharmAnalyzer")
    private String libelle;

    @OneToMany(mappedBy = "labPharm")
    private Collection<DispositifMedical> dispositifs;

    public LaboratoirePharmacologique(){}

    public LaboratoirePharmacologique(String libelle){ this.libelle = libelle; }

    public long getIdLabPharm() { return idLabPharm; }

    public void setIdLabPharm(long idLabPharm) { this.idLabPharm = idLabPharm; }

    public String getLibelle() { return libelle; }

    public void setLibelle(String libelle) { this.libelle = libelle; }

    public Collection<DispositifMedical> getDispositifs() { return dispositifs; }

    public void setDispositifs(Collection<DispositifMedical> dispositifs) { this.dispositifs = dispositifs; }
}


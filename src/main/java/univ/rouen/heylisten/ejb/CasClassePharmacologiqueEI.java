package univ.rouen.heylisten.ejb;

import javax.persistence.*;

@Entity
public class CasClassePharmacologiqueEI {
    @Id
    @GeneratedValue
    private long idCasClassePharmacologiqueEI;

    @ManyToOne
    @JoinColumn(name = "idEffetIndesirable")
    private EffetIndesirable effetIndesirableClassePharmacologique;

    @ManyToOne
    @JoinColumn(name = "idClassePharmacologique")
    private ClassePharmacologique classePharmacologique;

    public CasClassePharmacologiqueEI() {
    }

    public CasClassePharmacologiqueEI(EffetIndesirable effetIndesirableClassePharmacologique, ClassePharmacologique classePharmacologique) {
        this.effetIndesirableClassePharmacologique = effetIndesirableClassePharmacologique;
        this.classePharmacologique = classePharmacologique;
    }

    public EffetIndesirable getEffetIndesirableClassePharmacologique() {
        return effetIndesirableClassePharmacologique;
    }

    public void setEffetIndesirableClassePharmacologique(EffetIndesirable effetIndesirableClassePharmacologique) {
        this.effetIndesirableClassePharmacologique = effetIndesirableClassePharmacologique;
    }

    public ClassePharmacologique getClassePharmacologique() {
        return classePharmacologique;
    }

    public void setClassePharmacologique(ClassePharmacologique classePharmacologique) {
        this.classePharmacologique = classePharmacologique;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CasClassePharmacologiqueEI{");
        sb.append("idCasClassePharmacologiqueEI=").append(idCasClassePharmacologiqueEI);
        sb.append(", effetIndesirableClassePharmacologique=").append(effetIndesirableClassePharmacologique);
        sb.append(", classePharmacologique=").append(classePharmacologique);
        sb.append('}');
        return sb.toString();
    }
}

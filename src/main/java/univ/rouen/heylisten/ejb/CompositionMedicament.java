package univ.rouen.heylisten.ejb;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"idMedicament", "idSubstance"}))
public class CompositionMedicament implements Serializable {
    @Id
    @GeneratedValue
    private long idCompositionMedicament;

    @ManyToOne
    @JoinColumn(name = "idMedicament")
    private Medicament medicament;

    @ManyToOne
    @JoinColumn(name = "idSubstance")
    private SubstanceActive substanceActive;

    private String libelle;

    public CompositionMedicament() {
    }

    public CompositionMedicament(Medicament medicament, SubstanceActive substanceActive, String libelle) {
        this.medicament = medicament;
        this.substanceActive = substanceActive;
        this.libelle = libelle;
    }

    public Medicament getMedicament() {
        return medicament;
    }

    public void setMedicament(Medicament medicament) {
        this.medicament = medicament;
    }

    public SubstanceActive getSubstanceActive() {
        return substanceActive;
    }

    public void setSubstanceActive(SubstanceActive substanceActive) {
        this.substanceActive = substanceActive;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CompositionMedicament{");
        sb.append("medicament=").append(medicament);
        sb.append(", substanceActive=").append(substanceActive);
        sb.append(", libelle='").append(libelle).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

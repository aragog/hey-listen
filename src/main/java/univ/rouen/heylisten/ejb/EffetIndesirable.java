package univ.rouen.heylisten.ejb;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity
public class EffetIndesirable implements Serializable {
    @Id
    @GeneratedValue
    private long idEffetIndesirable;

    private String libelle;

    @OneToMany(mappedBy = "effetIndesirableClassePharmacologique")
    private Collection<CasClassePharmacologiqueEI> casClassePharmacologiqueEI;

    @OneToMany(mappedBy = "effetIndesirableClasseChimique")
    private Collection<CasClasseChimiqueEI> casClasseChimiqueEI;

    @OneToMany(mappedBy = "effetIndesirableSubstance")
    private Collection<CasSubstanceEI> casSubstanceEIs;

    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date premiereDeclaration;

    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date derniereDeclaration;

    public EffetIndesirable() {}

    public EffetIndesirable(String libelle) {
        this.libelle = libelle;
    }

    public long getIdEffetIndesirable() {
        return idEffetIndesirable;
    }

    public void setIdEffetIndesirable(long idEffetIndesirable) {
        this.idEffetIndesirable = idEffetIndesirable;
    }

    public Collection<CasClassePharmacologiqueEI> getCasClassePharmacologiqueEI() {
        return casClassePharmacologiqueEI;
    }

    public void setCasClassePharmacologiqueEI(Collection<CasClassePharmacologiqueEI> casClassePharmacologiqueEI) {
        this.casClassePharmacologiqueEI = casClassePharmacologiqueEI;
    }

    public Collection<CasClasseChimiqueEI> getCasClasseChimiqueEI() {
        return casClasseChimiqueEI;
    }

    public void setCasClasseChimiqueEI(Collection<CasClasseChimiqueEI> casClasseChimiqueEI) {
        this.casClasseChimiqueEI = casClasseChimiqueEI;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Collection<CasSubstanceEI> getCasSubstanceEIs() {
        return casSubstanceEIs;
    }

    public void setCasSubstanceEIs(Collection<CasSubstanceEI> casSubstanceEIs) {
        this.casSubstanceEIs = casSubstanceEIs;
    }

    public Date getPremiereDeclaration() {
        return premiereDeclaration;
    }

    public void setPremiereDeclaration(Date premiereDeclaration) {
        this.premiereDeclaration = premiereDeclaration;
    }

    public Date getDerniereDeclaration() {
        return derniereDeclaration;
    }

    public void setDerniereDeclaration(Date derniereDeclaration) {
        this.derniereDeclaration = derniereDeclaration;
    }

    @Override
    public String toString() {
        return this.libelle;
    }
}


package univ.rouen.heylisten.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import univ.rouen.heylisten.ejb.LaboratoirePharmacologique;
import univ.rouen.heylisten.services.LaboratoirePharmacologiqueService;

import java.text.ParseException;
import java.util.Locale;

@Component
public class LaboratoirePharmacologiqueConverter implements Formatter<LaboratoirePharmacologique> {
    @Autowired
    private LaboratoirePharmacologiqueService laboratoirePharmacologiqueService;

    @Override
    public LaboratoirePharmacologique parse(String text, Locale locale) throws ParseException {
        return laboratoirePharmacologiqueService.getById(Long.valueOf(text));
    }

    @Override
    public String print(LaboratoirePharmacologique object, Locale locale) {
        return object.getLibelle();
    }
}

package univ.rouen.heylisten.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import univ.rouen.heylisten.ejb.Medicament;
import univ.rouen.heylisten.services.MedicamentService;

import java.text.ParseException;
import java.util.Locale;

@Component
public class MedicamentConverter implements Formatter<Medicament> {
    @Autowired
    private MedicamentService medicamentService;

    @Override
    public Medicament parse(String text, Locale locale) throws ParseException {
        return medicamentService.getById(Long.valueOf(text));
    }

    @Override
    public String print(Medicament object, Locale locale) {
        return object.getLibelle();
    }
}

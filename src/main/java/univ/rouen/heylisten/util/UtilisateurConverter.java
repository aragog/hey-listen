package univ.rouen.heylisten.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import univ.rouen.heylisten.ejb.Utilisateur;
import univ.rouen.heylisten.services.UtilisateurService;

import java.text.ParseException;
import java.util.Locale;

@Component
public class UtilisateurConverter implements Formatter<Utilisateur> {
    @Autowired
    private UtilisateurService utilisateurService;

    @Override
    public Utilisateur parse(String text, Locale locale) throws ParseException {
        return utilisateurService.getById(Long.valueOf(text));
    }

    @Override
    public String print(Utilisateur object, Locale locale) {
        return object.toString();
    }
}

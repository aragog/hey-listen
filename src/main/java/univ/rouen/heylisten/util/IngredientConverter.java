package univ.rouen.heylisten.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import univ.rouen.heylisten.ejb.Ingredient;
import univ.rouen.heylisten.services.IngredientService;

import java.text.ParseException;
import java.util.Locale;

@Component
public class IngredientConverter implements Formatter<Ingredient> {
    @Autowired
    private IngredientService ingredientService;

    @Override
    public Ingredient parse(String text, Locale locale) throws ParseException {
        return ingredientService.getById(Long.valueOf(text));
    }

    @Override
    public String print(Ingredient object, Locale locale) {
        return object.getLibelle();
    }
}

package univ.rouen.heylisten.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import univ.rouen.heylisten.ejb.ClassePharmacologique;
import univ.rouen.heylisten.services.ClassePharmacologiqueService;

import java.text.ParseException;
import java.util.Locale;

@Component
public class ClassePharmacologiqueConverter implements Formatter<ClassePharmacologique> {
    @Autowired
    private ClassePharmacologiqueService classePharmacologiqueService;

    @Override
    public ClassePharmacologique parse(String text, Locale locale) throws ParseException {
        return classePharmacologiqueService.getById(Long.valueOf(text));
    }

    @Override
    public String print(ClassePharmacologique object, Locale locale) {
        return object.getLibelle();
    }
}

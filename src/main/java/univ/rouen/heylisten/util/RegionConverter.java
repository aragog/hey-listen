package univ.rouen.heylisten.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import univ.rouen.heylisten.ejb.Region;
import univ.rouen.heylisten.services.RegionService;

import java.text.ParseException;
import java.util.Locale;

@Component
public class RegionConverter implements Formatter<Region> {
    @Autowired
    private RegionService regionService;

    @Override
    public Region parse(String text, Locale locale) throws ParseException {
        return regionService.getById(Long.valueOf(text));
    }

    @Override
    public String print(Region object, Locale locale) {
        return object.getLibelle();
    }
}

package univ.rouen.heylisten.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import univ.rouen.heylisten.ejb.ProduitCosmetique;
import univ.rouen.heylisten.services.ProduitCosmetiqueService;

import java.text.ParseException;
import java.util.Locale;

@Component
public class ProduitCosmetiqueConverter implements Formatter<ProduitCosmetique> {
    @Autowired
    private ProduitCosmetiqueService produitCosmetiqueService;

    @Override
    public ProduitCosmetique parse(String text, Locale locale) throws ParseException {
        return produitCosmetiqueService.getByIdFull(Long.valueOf(text));
    }

    @Override
    public String print(ProduitCosmetique object, Locale locale) {
        return object.getLibelle();
    }
}

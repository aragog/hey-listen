package univ.rouen.heylisten.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import univ.rouen.heylisten.ejb.SubstanceActive;
import univ.rouen.heylisten.services.SubstanceActiveService;

import java.text.ParseException;
import java.util.Locale;

@Component
public class SubstanceActiveConverter implements Formatter<SubstanceActive> {
    @Autowired
    private SubstanceActiveService substanceActiveService;

    @Override
    public SubstanceActive parse(String text, Locale locale) throws ParseException {
        return substanceActiveService.getById(Long.valueOf(text));
    }

    @Override
    public String print(SubstanceActive object, Locale locale) {
        return object.toString();
    }
}

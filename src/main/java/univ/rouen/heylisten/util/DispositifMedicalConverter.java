package univ.rouen.heylisten.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import univ.rouen.heylisten.ejb.DispositifMedical;
import univ.rouen.heylisten.services.DispositifMedicalService;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by hieu on 21/01/15.
 */
@Component
public class DispositifMedicalConverter implements Formatter<DispositifMedical> {
    @Autowired
    private DispositifMedicalService dispositifMedicalService;

    @Override
    public DispositifMedical parse(String text, Locale locale) throws ParseException {
        return dispositifMedicalService.getById(Long.valueOf(text));
    }

    @Override
    public String print(DispositifMedical object, Locale locale) {
        return object.getLibelle();
    }
}

package univ.rouen.heylisten.util;

/**
 * Type d'utilisateur pouvant utiliser l'application.
 * <ul>
 *     <li>Utilisateur lambda</li>
 *     <li>Patient suivant un traitement en cours</li>
 *     <li>Association de patients</li>
 *     <li>Médecin hospitalier</li>
 *     <li>Médecin de ville</li>
 *     <li>Laboratoire pharmaceutique</li>
 *     <li>Laboratoire de cosmétiques</li>
 *     <li>Centre Régional de Pharmaco Vigilance</li>
 *     <li>Centre National de Pharmaco Vigilance</li>
 *     <li>Administrateur</li>
 * </ul>
 */
public enum UserType {
    // Les valeurs à zéro sont des valeurs inconnues (aucun impact sur le poids)
    UTILISATEUR(0.5, 1.), PATIENT(0.75, 0), ASSOCIATION(1., 0), MEDECIN_VILLE(1.25, 0),
    MEDECIN_HOPITAL(1.5, 0), LABO_PHARMA(0, 0), LABO_COSMETIQUE(0, 0),
    CRPV(2., 2.), CNPV(3., 2.5), ADMIN(0, 0);

    private final double pMedocOuDispoMedical;
    private final double pProdCosmet;

    UserType(double p1, double p2) {
        this.pMedocOuDispoMedical = p1;
        this.pProdCosmet = p2;
    }

    public double poidsMedicamentDispoMedical() {
        return this.pMedocOuDispoMedical;
    }

    public double poidsProdCosmetique() {
        return this.pProdCosmet;
    }
}

package univ.rouen.heylisten.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import univ.rouen.heylisten.ejb.LaboratoireCosmetique;
import univ.rouen.heylisten.services.LaboratoireCosmetiqueService;

import java.text.ParseException;
import java.util.Locale;

@Component
public class LaboratoireCosmetiqueConverter implements Formatter<LaboratoireCosmetique> {
    @Autowired
    private LaboratoireCosmetiqueService laboratoireCosmetiqueService;

    @Override
    public LaboratoireCosmetique parse(String text, Locale locale) throws ParseException {
        return laboratoireCosmetiqueService.getById(Long.valueOf(text));
    }

    @Override
    public String print(LaboratoireCosmetique object, Locale locale) {
        return object.getLibelle();
    }
}

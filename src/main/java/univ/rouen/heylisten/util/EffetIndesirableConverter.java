package univ.rouen.heylisten.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import univ.rouen.heylisten.ejb.NotificationMedicament;
import univ.rouen.heylisten.services.NotificationMedicamentService;

import java.text.ParseException;
import java.util.Locale;

@Component
public class EffetIndesirableConverter implements Formatter<NotificationMedicament>{
    @Autowired
    private NotificationMedicamentService notificationMedicamentService;

    @Override
    public NotificationMedicament parse(String text, Locale locale) throws ParseException {
        return notificationMedicamentService.getById(Long.valueOf(text));
    }

    @Override
    public String print(NotificationMedicament object, Locale locale) {
        return object.toString();
    }
}

package univ.rouen.heylisten.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import univ.rouen.heylisten.ejb.ClasseChimique;
import univ.rouen.heylisten.services.ClasseChimiqueService;

import java.text.ParseException;
import java.util.Locale;

@Component
public class ClasseChimiqueConverter implements Formatter<ClasseChimique> {
    @Autowired
    private ClasseChimiqueService classeChimiqueService;

    @Override
    public ClasseChimique parse(String text, Locale locale) throws ParseException {
        return classeChimiqueService.getById(Long.valueOf(text));
    }

    @Override
    public String print(ClasseChimique object, Locale locale) {
        return object.getLibelle();
    }
}

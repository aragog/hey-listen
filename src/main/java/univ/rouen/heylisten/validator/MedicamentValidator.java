package univ.rouen.heylisten.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import univ.rouen.heylisten.ejb.Medicament;
import univ.rouen.heylisten.services.MedicamentService;

@Component
public class MedicamentValidator implements Validator {
    @Autowired
    private MedicamentService medicamentService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Medicament.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        final Medicament m = (Medicament) target;
        final Medicament old = medicamentService.getByCodeCis(m.getCode_cis());
        if (old != null) {
            if (m.getIdMedicament() > 0 && m.getIdMedicament() == old.getIdMedicament()) {
                return;
            }
            errors.rejectValue("code_cis", "code_cis.unique", "code CIS déjà pris");
        }
    }
}

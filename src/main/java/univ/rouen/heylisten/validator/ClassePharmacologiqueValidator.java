package univ.rouen.heylisten.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import univ.rouen.heylisten.ejb.ClassePharmacologique;
import univ.rouen.heylisten.services.ClassePharmacologiqueService;

@Component
public class ClassePharmacologiqueValidator implements Validator {
    @Autowired
    private ClassePharmacologiqueService classePharmacologiqueService;

    @Override
    public boolean supports(Class<?> clazz) {
        return ClassePharmacologique.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        final ClassePharmacologique c = (ClassePharmacologique) target;
        final ClassePharmacologique old1 = classePharmacologiqueService.getByIdClassePharmacologique(c.getIdClassePharmacologique());
        if (old1 != null) {
            if (c.getId() > 0 && c.getId() == old1.getId()) {
                return;
            }
            errors.rejectValue("idClassePharmacologique", "idClassePharmacologique.unique", "ID classe pharmacologique déjà pris");
        }

        final ClassePharmacologique old2 = classePharmacologiqueService.getByLibelle(c.getLibelle());
        if (old2 != null) {
            if (c.getId() > 0 && c.getId() == old2.getId()) {
                return;
            }
            errors.rejectValue("libelle", "libelle.unique", "Libellé classe pharmacologique déjà prise");
        }
    }
}
package univ.rouen.heylisten.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import univ.rouen.heylisten.ejb.ClasseChimique;
import univ.rouen.heylisten.services.ClasseChimiqueService;

@Component
public class ClasseChimiqueValidator implements Validator {
    @Autowired
    private ClasseChimiqueService classeChimiqueService;

    @Override
    public boolean supports(Class<?> clazz) {
        return ClasseChimique.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        final ClasseChimique c = (ClasseChimique) target;
        final ClasseChimique old1 = classeChimiqueService.getByIdClasseChimique(c.getIdClasseChimique());
        if (old1 != null) {
            if (c.getId() > 0 && c.getId() == old1.getId()) {
                return;
            }
            errors.rejectValue("idClasseChimique", "idClasseChimique.unique", "ID classe chimique déjà pris");
        }

        final ClasseChimique old2 = classeChimiqueService.getByLibelle(c.getLibelle());
        if (old2 != null) {
            if (c.getId() > 0 && c.getId() == old2.getId()) {
                return;
            }
            errors.rejectValue("libelle", "libelle.unique", "Libellé classe chimique déjà prise");
        }
    }
}
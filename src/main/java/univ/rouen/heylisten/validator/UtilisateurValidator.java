package univ.rouen.heylisten.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import univ.rouen.heylisten.ejb.Utilisateur;
import univ.rouen.heylisten.services.UtilisateurService;

@Component
public class UtilisateurValidator implements Validator {
    @Autowired
    private UtilisateurService utilisateurService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Utilisateur.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        final Utilisateur u = (Utilisateur) target;
        final Utilisateur old = utilisateurService.getByUsername(u.getUsername());
        if (old != null) {
            // user with this username
            if (u.getIdUtilisateur() > 0 && u.getIdUtilisateur() == old.getIdUtilisateur()) {
                return;
            }
            errors.rejectValue("username", "username.unique", "nom d'utilisateur déjà pris");
        }
    }
}

package univ.rouen.heylisten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import univ.rouen.heylisten.ejb.ClasseChimique;
import univ.rouen.heylisten.exceptions.HasParentsException;
import univ.rouen.heylisten.services.ClasseChimiqueService;
import univ.rouen.heylisten.validator.ClasseChimiqueValidator;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/classeChimique")
@SessionAttributes({"classesChimiques", "classeChimique"})
public class ClasseChimiqueController {

    @Autowired
    ClasseChimiqueService classeChimiqueService;
    @Autowired
    private ClasseChimiqueValidator classeChimiqueValidator;

    @InitBinder({"newclasseChimique", "classeChimique"})
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(classeChimiqueValidator);
    }

    @ModelAttribute("classesChimiques")
    public List<ClasseChimique> populateModelWithClassesChimiques() {
        return classeChimiqueService.getAllOrderedById();
    }

    @RequestMapping("all")
    public String all(Model model) throws Exception {

        model.addAttribute("classesChimiques", classeChimiqueService.getAllOrderedById());
        return "classeChimique/all";
    }

    @RequestMapping("detail")
    public String detail(@RequestParam("id") Long id, Model model) {
        model.addAttribute("classeChimique", classeChimiqueService.getById(id));
        return "classeChimique/details";
    }

    @RequestMapping("creer")
    public String creer(@ModelAttribute("newclasseChimique") ClasseChimique classeChimique) {
        return "classeChimique/create";
    }

    @RequestMapping("/update/{classeChimique}")
    public String update(@ModelAttribute("classeChimique") ClasseChimique classeChimique) {
        return "classeChimique/update";
    }

    @RequestMapping("ajouter")
    public String ajouter(@Valid @ModelAttribute("newclasseChimique") ClasseChimique classeChimique,
                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "classeChimique/create";
        }
        classeChimiqueService.create(classeChimique);
        return "redirect:/classeChimique/all";
    }

    @RequestMapping("/update/miseajour")
    public String update(@Valid @ModelAttribute("classeChimique") ClasseChimique classeChimique,
                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "classeChimique/update";
        }
        classeChimiqueService.update(classeChimique);
        return "redirect:/classeChimique/all";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam("id") Long id, RedirectAttributes redirectAttrs) {
        if (classeChimiqueService.hasAnyParents(id)) {
            redirectAttrs.addFlashAttribute("error",
                    new HasParentsException("Cette classe est attach&eacute;e"
                            + " &agrave; une autre classe!"));
        } else {
            classeChimiqueService.delete(id);
        }
        return "redirect:/classeChimique/all";
    }

    @RequestMapping("search")
    public String search(@ModelAttribute("searchText") String searchText, Model model) throws Exception {
        model.addAttribute("classesChimiquesRecherche", classeChimiqueService.search(searchText));
        return "classeChimique/search";
    }

/*
    @RequestMapping("search")
    public String search( @RequestParam(required= false, defaultValue="") String searchText, Model model) throws Exception {
        model.addAttribute("classesChimiquesRecherche", classeChimiqueService.search(searchText));
        return "classeChimique/all";
    }
    */
}

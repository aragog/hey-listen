package univ.rouen.heylisten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import univ.rouen.heylisten.ejb.ClassePharmacologique;
import univ.rouen.heylisten.exceptions.HasParentsException;
import univ.rouen.heylisten.services.ClassePharmacologiqueService;
import univ.rouen.heylisten.validator.ClassePharmacologiqueValidator;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/classePharmacologique")
@SessionAttributes({"classesPharmacologiques", "classePharmacologique"})
public class ClassePharmacologiqueController {

    @Autowired
    ClassePharmacologiqueService classePharmacologiqueService;
    @Autowired
    private ClassePharmacologiqueValidator classePharmacologiqueValidator;

    @InitBinder({"newclassePharmacologique", "classePharmacologique"})
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(classePharmacologiqueValidator);
    }

    @ModelAttribute("classesPharmacologiques")
    public List<ClassePharmacologique> populateModelWithClassesPharmacologiques() {
        return classePharmacologiqueService.getAllOrderedById();
    }

    @RequestMapping("all")
    public String all(Model model) {
        model.addAttribute("classesPharmacologiques", classePharmacologiqueService.getAllOrderedById());
        return "classePharmacologique/all";
    }

    @RequestMapping("detail")
    public String detail(@RequestParam("id") Long id, Model model) {
        model.addAttribute("classePharmacologique", classePharmacologiqueService.getById(id));
        return "classePharmacologique/details";
    }

    @RequestMapping("creer")
    public String creer(@ModelAttribute("newclassePharmacologique") ClassePharmacologique classePharmacologique) {
        return "classePharmacologique/create";
    }

    @RequestMapping("/update/{classePharmacologique}")
    public String update(@ModelAttribute("classePharmacologique") ClassePharmacologique classePharmacologique) {
        return "classePharmacologique/update";
    }

    @RequestMapping("ajouter")
    public String ajouter(@Valid @ModelAttribute("newclassePharmacologique") ClassePharmacologique classePharmacologique,
                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "classePharmacologique/create";
        }
        classePharmacologiqueService.create(classePharmacologique);
        return "redirect:/classePharmacologique/all";
    }

    @RequestMapping("/update/miseajour")
    public String update(@Valid @ModelAttribute("classePharmacologique") ClassePharmacologique classePharmacologique,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "classePharmacologique/update";
        }
        classePharmacologiqueService.update(classePharmacologique);
        return "redirect:/classePharmacologique/all";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam("id") Long id, RedirectAttributes redirectAttrs) {
        if (classePharmacologiqueService.hasAnyParents(id)) {
            redirectAttrs.addFlashAttribute("error",
                    new HasParentsException("Cette classe est attach&eacute;e"
                            + " &agrave; une autre classe!"));
        } else {
            classePharmacologiqueService.delete(id);
        }
        return "redirect:/classePharmacologique/all";
    }

    @RequestMapping("search")
    public String search(@ModelAttribute("searchText") String searchText, Model model) throws Exception {
        model.addAttribute("classesPharmacologiquesRecherche", classePharmacologiqueService.search(searchText));
        return "classePharmacologique/search";
    }


  /*  @RequestMapping("search")
    public String search(@RequestParam(required= false, defaultValue="") String libelle, Model model)
    {
        model.addAttribute("classesPharmacologiquesRecherche", classePharmacologiqueService.search(libelle.trim()));
        return "classePharmacologique/all";
    }*/
}

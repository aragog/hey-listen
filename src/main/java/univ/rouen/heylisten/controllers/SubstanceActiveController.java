package univ.rouen.heylisten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import univ.rouen.heylisten.ejb.SubstanceActive;
import univ.rouen.heylisten.exceptions.HasParentsException;
import univ.rouen.heylisten.services.SubstanceActiveService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/substanceActive")
@SessionAttributes({"substancesActives", "substanceActive"})
public class SubstanceActiveController {
    @Autowired
    private SubstanceActiveService substanceActiveService;

    @ModelAttribute("substancesActives")
    public List<SubstanceActive> populateModelWithLaboratoiresCosmetiques() {
        return substanceActiveService.getAllOrderedByLibelle();
    }

    @RequestMapping("all")
    public String all(Model model) {
        model.addAttribute("substancesActives", substanceActiveService.getAllOrderedByLibelle());
        return "substanceActive/all";
    }

    @RequestMapping("detail")
    public String detail(@RequestParam("id") Long id, Model model) {
        model.addAttribute("substanceActive", substanceActiveService.getById(id));
        return "substanceActive/details";
    }

    @RequestMapping("creer")
    public String creer(@ModelAttribute("newsubstanceActive")SubstanceActive substanceActive) {
        return "substanceActive/create";
    }

    @RequestMapping("/update/{substanceActive}")
    public String update(@ModelAttribute("substanceActive") SubstanceActive substanceActive) {
        return "substanceActive/update";
    }

    @RequestMapping("ajouter")
    public String ajouter(@Valid @ModelAttribute("newsubstanceActive") SubstanceActive substanceActive,
                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "substanceActive/create";
        }
        substanceActiveService.create(substanceActive);
        return "redirect:/substanceActive/all";
    }

    @RequestMapping("/update/miseajour")
    public String update(@Valid @ModelAttribute("substanceActive") SubstanceActive substanceActive,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "substanceActive/update";
        }
        substanceActiveService.update(substanceActive);
        return "redirect:/substanceActive/all";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam("id") Long id, RedirectAttributes redirectAttrs) {
        if (substanceActiveService.hasAnyParents(id)) {
            redirectAttrs.addFlashAttribute("error",
                    new HasParentsException("Cette substance est encore utilis&eacute;e"));
        } else {
            substanceActiveService.delete(id);
        }
        return "redirect:/substanceActive/all";
    }

    @RequestMapping("search")
    public String search(@ModelAttribute("searchText") String searchText, Model model) throws Exception {
        model.addAttribute("substancesActivesRecherche", substanceActiveService.search(searchText));
        return "substanceActive/search";
    }

    /*
    @RequestMapping("search")
    public String search(@RequestParam(required= false, defaultValue="") String libelle, Model model)
    {
        model.addAttribute("substancesActivesRecherche", substanceActiveService.search(libelle.trim()));
        return "substanceActive/all";
    }
    */
}

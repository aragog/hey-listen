package univ.rouen.heylisten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import univ.rouen.heylisten.ejb.*;
import univ.rouen.heylisten.services.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/notification")
@SessionAttributes({"medicaments", "produits", "dispositifs"})
public class NotificationController {
    @Autowired
    private CasSubstanceEIService casSubstanceEIService;
    @Autowired
    private CasClasseChimiqueEIService casClasseChimiqueEIService;
    @Autowired
    private CasClassePharmacologiqueEIService casClassePharmacologiqueEIService;
    @Autowired
    private MedicamentService medicamentService;
    @Autowired
    private EffetIndesirableService effetIndesirableService;
    @Autowired
    private NotificationMedicamentService notificationMedicamentService;
    @Autowired
    private UtilisateurService utilisateurService;
    @Autowired
    private NotificationProduitService notificationProduitService;
    @Autowired
    private ProduitCosmetiqueService produitCosmetiqueService;
    @Autowired
    private NotificationDispositifService notificationDispositifService;
    @Autowired
    private DispositifMedicalService dispositifMedicalService;

    @ModelAttribute("medicaments")
    public List<Medicament> populateModelWithMedicaments() {
        return medicamentService.getAllOrderedByLibelle();
    }

    @ModelAttribute("produits")
    public List<ProduitCosmetique> populateModelWithProduits() {
        return produitCosmetiqueService.getAllOrderedByLibelle();
    }

    @ModelAttribute("dispositifs")
    public List<DispositifMedical> populateModelWithDispositifs() {
        return dispositifMedicalService.getAllOrderedByLibelle();
    }

    @RequestMapping("/allSubstance")
    public String allSubstance(Model model) {
        model.addAttribute("casSubstanceEI", casSubstanceEIService.getAll());
        return "notification/allSubstance";
    }

    @RequestMapping("/allChimique")
    public String allChimique(Model model) {
        model.addAttribute("casChimiqueEI", casClasseChimiqueEIService.getAll());
        return "notification/allChimique";
    }

    @RequestMapping("/allPharmacologique")
    public String allPharmacologique(Model model) {
        model.addAttribute("casPharmacologiqueEI", casClassePharmacologiqueEIService.getAll());
        return "notification/allPharmacologique";
    }

    @RequestMapping("/allMedicament")
    public String allMedicament(Model model) {
        model.addAttribute("notificationsMedicaments", notificationMedicamentService.getAll());
        return "notification/allMedicament";
    }

    @RequestMapping("/allProduit")
    public String allProduit(Model model) {
        model.addAttribute("notificationsProduits", notificationProduitService.getAll());
        return "notification/allProduit";
    }

    @RequestMapping("/allDispositif")
    public String allDispositif(Model model) {
        model.addAttribute("notificationsDispositifs", notificationDispositifService.getAll());
        return "notification/allDispositif";
    }

    @RequestMapping("/creerNotificationMedicament")
    public String creerNotificationMedicament(@ModelAttribute("newNotificationMedicament")NotificationMedicament notificationMedicament) {
        return "notification/createNotificationMedicament";
    }

    @RequestMapping("/ajouterNotificationMedicament")
    public String ajouterNotificationMedicament(@Valid @ModelAttribute("newNotificationMedicament") NotificationMedicament notificationMedicament,
                                                BindingResult bindingResult,
                                                Principal principal) {
        if (bindingResult.hasErrors()) {
            return "notification/createNotificationMedicament";
        }
        notificationMedicament.setDateNotif(new Date());
        notificationMedicament.getEffetIndesirable().setPremiereDeclaration(new Date());
        notificationMedicament.getEffetIndesirable().setDerniereDeclaration(new Date());
        notificationMedicament.setUtilisateur(getCurrentUser(principal));
        effetIndesirableService.create(notificationMedicament.getEffetIndesirable());
        notificationMedicamentService.create(notificationMedicament);
        return "redirect:/notification/allMedicament";
    }

    @RequestMapping("/creerNotificationProduitCosmetique")
    public String creerNotificationProduitCosmetique(@ModelAttribute("newNotificationProduitCosmetique")NotificationProduit notificationProduit) {
        return "notification/createNotificationProduit";
    }

    @RequestMapping("/ajouterNotificationProduitCosmetique")
    public String ajouterNotificationProduitCosmetique(@Valid @ModelAttribute("newNotificationProduitCosmetique") NotificationProduit notificationProduit,
                                                BindingResult bindingResult,
                                                Principal principal) {
        if (bindingResult.hasErrors()) {
            return "notification/createNotificationProduit";
        }
        notificationProduit.setDateNotif(new Date());
        notificationProduit.getEffetIndesirable().setPremiereDeclaration(new Date());
        notificationProduit.getEffetIndesirable().setDerniereDeclaration(new Date());
        notificationProduit.setUtilisateur(getCurrentUser(principal));
        effetIndesirableService.create(notificationProduit.getEffetIndesirable());
        notificationProduitService.create(notificationProduit);
        return "redirect:/notification/allProduit";
    }

    @RequestMapping("/creerNotificationDispositifMedical")
    public String creerNotificationDispositifMedical(@ModelAttribute("newNotificationDispositifMedical")NotificationDispositif notificationDispositif) {
        return "notification/createNotificationDispositif";
    }

    @RequestMapping("/ajouterNotificationDispositifMedical")
    public String ajouterNotificationDispositifMedical(@Valid @ModelAttribute("newNotificationDispositifMedical") NotificationDispositif notificationDispositif,
                                                       BindingResult bindingResult,
                                                       Principal principal) {
        if (bindingResult.hasErrors()) {
            return "notification/createNotificationDispositif";
        }
        notificationDispositif.setDateNotif(new Date());
        notificationDispositif.getEffetIndesirable().setPremiereDeclaration(new Date());
        notificationDispositif.getEffetIndesirable().setDerniereDeclaration(new Date());
        notificationDispositif.setUtilisateur(getCurrentUser(principal));
        effetIndesirableService.create(notificationDispositif.getEffetIndesirable());
        notificationDispositifService.create(notificationDispositif);
        return "redirect:/notification/allDispositif";
    }


    @RequestMapping("/deleteNotificationMedicament")
    public String deleteNotificationMedicament(@RequestParam("id") Long id) {
        notificationMedicamentService.delete(id);
        return "redirect:/notification/allMedicament";
    }

    @RequestMapping("/deleteNotificationProduit")
    public String deleteNotificationProduit(@RequestParam("id") Long id) {
        notificationProduitService.delete(id);
        return "redirect:/notification/allProduit";
    }

    @RequestMapping("/deleteNotificationDispositif")
    public String deleteNotificationDispositif(@RequestParam("id") Long id) {
        notificationDispositifService.delete(id);
        return "redirect:/notification/allDispositif";
    }

    @RequestMapping("/deleteEISubstance")
    public String delete(@RequestParam("id") Long id) {
        casSubstanceEIService.delete(id);
        return "redirect:/notification/allSubstance";
    }

    // OUTILS
    private Utilisateur getCurrentUser(Principal principal) {
        return utilisateurService.getByUsername(principal.getName());
    }
}

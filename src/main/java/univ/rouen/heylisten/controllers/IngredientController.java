package univ.rouen.heylisten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import univ.rouen.heylisten.ejb.Ingredient;
import univ.rouen.heylisten.exceptions.HasParentsException;
import univ.rouen.heylisten.services.IngredientService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/ingredient")
@SessionAttributes({"ingredients", "ingredient"})
public class IngredientController {
    @Autowired
    private IngredientService ingredientService;

    @ModelAttribute("ingredients")
    public List<Ingredient> populateModelWithIngredients() {
        return ingredientService.getAllOrderedByLibelle();
    }

    @RequestMapping("all")
    public String all(Model model) {
        model.addAttribute("ingredients", ingredientService.getAllOrderedByLibelle());
        return "ingredient/all";
    }

    @RequestMapping("detail")
    public String detail(@RequestParam("id") Long id, Model model) {
        model.addAttribute("ingredient", ingredientService.getById(id));
        return "ingredient/details";
    }

    @RequestMapping("creer")
    public String creer(@ModelAttribute("newingredient") Ingredient ingredient) {
        return "ingredient/create";
    }

    @RequestMapping("/update/{ingredient}")
    public String update(@ModelAttribute("ingredient") Ingredient ingredient) {
        return "ingredient/update";
    }

    @RequestMapping("ajouter")
    public String ajouter(@Valid @ModelAttribute("newingredient") Ingredient ingredient,
                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "ingredient/create";
        }
        ingredientService.create(ingredient);
        return "redirect:/ingredient/all";
    }

    @RequestMapping("/update/miseajour")
    public String update(@Valid @ModelAttribute("ingredient") Ingredient ingredient,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "ingredient/update";
        }
        ingredientService.update(ingredient);
        return "redirect:/ingredient/all";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam("id") Long id, RedirectAttributes redirectAttrs) {
        if (ingredientService.hasAnyParents(id)) {
            redirectAttrs.addFlashAttribute("error",
                    new HasParentsException("Cet ingr&eacute;dient est encore attach&eacute;"
                            + " &agrave; un produit cosm&eacute;tique"));
        } else {
            ingredientService.delete(id);
        }
        return "redirect:/ingredient/all";
    }

    @RequestMapping("search")
    public String search(@ModelAttribute("searchText") String searchText, Model model) throws Exception {
        model.addAttribute("ingredientsRecherche", ingredientService.search(searchText));
        return "ingredient/search";
    }

    /*
    @RequestMapping("search")
    public String search(@RequestParam(required= false, defaultValue="") String libelle, Model model)
    {
        model.addAttribute("ingredientsRecherche", ingredientService.search(libelle.trim()));
        return "ingredient/all";
    }
    */
}
package univ.rouen.heylisten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import univ.rouen.heylisten.ejb.LaboratoireCosmetique;
import univ.rouen.heylisten.exceptions.HasParentsException;
import univ.rouen.heylisten.services.LaboratoireCosmetiqueService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/laboratoireCosmetique")
@SessionAttributes({"laboratoiresCosmetiques", "laboratoireCosmetique"})
public class LaboratoireCosmetiqueController {
    @Autowired
    private LaboratoireCosmetiqueService laboratoireCosmetiqueService;

    @ModelAttribute("laboratoiresCosmetiques")
    public List<LaboratoireCosmetique> populateModelWithLaboratoiresCosmetiques() {
        return laboratoireCosmetiqueService.getAllOrderedByLibelle();
    }

    @RequestMapping("all")
    public String all(Model model) {
        model.addAttribute("laboratoiresCosmetiques", laboratoireCosmetiqueService.getAllOrderedByLibelle());
        return "laboratoireCosmetique/all";
    }

    @RequestMapping("detail")
    public String detail(@RequestParam("id") Long id, Model model) {
        model.addAttribute("laboratoireCosmetique", laboratoireCosmetiqueService.getById(id));
        return "laboratoireCosmetique/details";
    }

    @RequestMapping("creer")
    public String creer(@ModelAttribute("newlaboratoireCosmetique") LaboratoireCosmetique laboratoireCosmetique) {
        return "laboratoireCosmetique/create";
    }

    @RequestMapping("/update/{laboratoireCosmetique}")
    public String update(@ModelAttribute("laboratoireCosmetique") LaboratoireCosmetique laboratoireCosmetique) {
        return "laboratoireCosmetique/update";
    }

    @RequestMapping("ajouter")
    public String ajouter(@Valid @ModelAttribute("newlaboratoireCosmetique") LaboratoireCosmetique laboratoireCosmetique,
                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "laboratoireCosmetique/create";
        }
        laboratoireCosmetiqueService.create(laboratoireCosmetique);
        return "redirect:/laboratoireCosmetique/all";
    }

    @RequestMapping("/update/miseajour")
    public String update(@Valid @ModelAttribute("laboratoireCosmetique") LaboratoireCosmetique laboratoireCosmetique,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "laboratoireCosmetique/update";
        }
        laboratoireCosmetiqueService.update(laboratoireCosmetique);
        return "redirect:/laboratoireCosmetique/all";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam("id") Long id, RedirectAttributes redirectAttrs) {
        if (laboratoireCosmetiqueService.hasAnyParents(id)) {
            redirectAttrs.addFlashAttribute("error",
                    new HasParentsException("Ce laboratoire est encore attach&eacute;"
                            + " &agrave; un produit cosm&eacute;tique"));
        } else {
            laboratoireCosmetiqueService.delete(id);
        }
        return "redirect:/laboratoireCosmetique/all";
    }

    @RequestMapping("search")
    public String search(@ModelAttribute("searchText") String searchText, Model model) throws Exception {
        model.addAttribute("laboratoiresCosmetiquesRecherche", laboratoireCosmetiqueService.search(searchText));
        return "laboratoireCosmetique/search";
    }

    /*
    @RequestMapping("search")
    public String search(@RequestParam(required= false, defaultValue="") String libelle, Model model)
    {
        model.addAttribute("laboratoiresCosmetiquesRecherche", laboratoireCosmetiqueService.search(libelle.trim()));
        return "laboratoireCosmetique/all";
    }
    */
}

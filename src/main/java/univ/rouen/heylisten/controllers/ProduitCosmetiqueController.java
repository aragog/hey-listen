package univ.rouen.heylisten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import univ.rouen.heylisten.ejb.Ingredient;
import univ.rouen.heylisten.ejb.LaboratoireCosmetique;
import univ.rouen.heylisten.ejb.ProduitCosmetique;
import univ.rouen.heylisten.services.IngredientService;
import univ.rouen.heylisten.services.LaboratoireCosmetiqueService;
import univ.rouen.heylisten.services.ProduitCosmetiqueService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/produitCosmetique")
@SessionAttributes({"produitsCosmetiques", "produitCosmetique", "ingredients"})
public class ProduitCosmetiqueController {
    @Autowired
    private ProduitCosmetiqueService produitCosmetiqueService;
    @Autowired
    private LaboratoireCosmetiqueService laboratoireCosmetiqueService;
    @Autowired
    private IngredientService ingredientService;

    @ModelAttribute("produitsCosmetiques")
    public List<ProduitCosmetique> populateModelWithProduitsCosmetiques() {
        return produitCosmetiqueService.getAllOrderedByLibelle();
    }

    @ModelAttribute("ingredients")
    public List<Ingredient> populateModelWithIngredients() {
        return ingredientService.getAllOrderedByLibelle();
    }

    @ModelAttribute("laboratoires")
    public List<LaboratoireCosmetique> populateModelWithLaboratoires() {
        return laboratoireCosmetiqueService.getAll();
    }

    @RequestMapping("all")
    public String all(Model model) {
        model.addAttribute("produitsCosmetiques", produitCosmetiqueService.getAllOrderedByLibelle());
        return "produitCosmetique/all";
    }

    @RequestMapping("detail")
    public String detail(@RequestParam("id") Long id, Model model) {
        model.addAttribute("produitCosmetique", produitCosmetiqueService.getByIdFull(id));
        return "produitCosmetique/details";
    }

    @RequestMapping("creer")
    public String creer(@ModelAttribute("newproduitCosmetique") ProduitCosmetique produitCosmetique) {
        return "produitCosmetique/create";
    }

    @RequestMapping("/update/{produitCosmetique}")
    public String update(@ModelAttribute("produitCosmetique") ProduitCosmetique produitCosmetique) {
        return "produitCosmetique/update";
    }

    @RequestMapping("ajouter")
    public String ajouter(@Valid @ModelAttribute("newproduitCosmetique") ProduitCosmetique produitCosmetique,
                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "produitCosmetique/create";
        }
        produitCosmetiqueService.create(produitCosmetique);
        return "redirect:/produitCosmetique/all";
    }

    @RequestMapping("/update/miseajour")
    public String update(@Valid @ModelAttribute("produitCosmetique") ProduitCosmetique produitCosmetique,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "produitCosmetique/update";
        }
        produitCosmetiqueService.update(produitCosmetique);
        return "redirect:/produitCosmetique/all";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam("id") Long id) {
        produitCosmetiqueService.delete(id);
        return "redirect:/produitCosmetique/all";
    }

    @RequestMapping("search")
    public String search(@ModelAttribute("searchText") String searchText, Model model) throws Exception {
        model.addAttribute("produitsCosmetiquesRecherche", produitCosmetiqueService.search(searchText));
        return "produitCosmetique/search";
    }

    /*
    @RequestMapping("search")
    public String search(@RequestParam(required= false, defaultValue="") String libelle, Model model)
    {
        model.addAttribute("produitsCosmetiquesRecherche", produitCosmetiqueService.search(libelle.trim()));
        return "produitCosmetique/all";
    }
    */
}

package univ.rouen.heylisten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import univ.rouen.heylisten.ejb.Region;
import univ.rouen.heylisten.ejb.Role;
import univ.rouen.heylisten.ejb.Utilisateur;
import univ.rouen.heylisten.form.UtilisateurFilter;
import univ.rouen.heylisten.services.RegionService;
import univ.rouen.heylisten.services.RoleService;
import univ.rouen.heylisten.services.UtilisateurService;
import univ.rouen.heylisten.util.UserType;
import univ.rouen.heylisten.validator.UtilisateurValidator;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Controller
@SessionAttributes({"userTypes", "regions", "filter", "user", "connectedUser"})
@RequestMapping("/utilisateur")
public class UtilisateurController {
    @Autowired
    private UtilisateurService utilisateurService;
    @Autowired
    private RegionService regionService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private RoleService roleService;
    @Autowired
    private UtilisateurValidator utilisateurValidator;

    @InitBinder({"newuser", "user"})
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(utilisateurValidator);
    }

    @ModelAttribute("userTypes")
    public List<UserType> populateModelWithUserTypes(Principal principal) {
        final Utilisateur current = getCurrentUser(principal);
        final List<UserType> userTypeList = new LinkedList<>();
        final UserType[] userTypes = UserType.values();
        for (UserType type : userTypes) {
            if (current.getType().ordinal() > type.ordinal()) {
                userTypeList.add(type);
            }
        }
        return userTypeList;
    }

    @ModelAttribute("regions")
    public List<Region> populateModelWithRegions(Principal principal) {
        final Utilisateur current = getCurrentUser(principal);
        if (current.getType().equals(UserType.CRPV)) {
            return Arrays.asList(regionService.getById(current.getRegion().getIdRegion()));
        }
        return regionService.getAll();
    }

    @ModelAttribute("filter")
    public UtilisateurFilter populateModelWithFilter(ModelMap modelMap) {
        return new UtilisateurFilter((List<UserType>) modelMap.get("userTypes"));
    }

    @ModelAttribute("connectedUser")
    public Utilisateur populateModelWithconnectedUser(Principal principal) {
        return getCurrentUser(principal);
    }

    @RequestMapping("all")
    public String allUtilisateurs(Model model, ModelMap modelMap) {
        final Utilisateur utilisateur = (Utilisateur) modelMap.get("connectedUser");
        model.addAttribute("users", utilisateurService
                .getAllByRegion(utilisateur.getRegion().getIdRegion(), utilisateur.getType()));
        return "utilisateur/all";
    }

    @RequestMapping("filter")
    public String filterByTypes(@ModelAttribute("filter") UtilisateurFilter filter,
                                Model model,
                                ModelMap modelMap) {
        final Utilisateur utilisateur = (Utilisateur) modelMap.get("connectedUser");
        model.addAttribute("users",
                utilisateurService.getAllByRegionAndUserType(filter.getSelectedTypes(),
                        utilisateur.getType(),
                        utilisateur.getRegion().getIdRegion()));
        return "utilisateur/all";
    }

    @RequestMapping("reinit")
    public String resetFilter(@ModelAttribute("filter") UtilisateurFilter filter,
                              @ModelAttribute("userTypes") UserType[] userTypes,
                              Model model,
                              ModelMap modelMap) {
        filter.setSelectedTypes((List<UserType>) modelMap.get("userTypes"));
        return filterByTypes(filter, model, modelMap);
    }

    @RequestMapping("selectNone")
    public String selectNoneFilter(@ModelAttribute("filter") UtilisateurFilter filter,
                                   Model model,
                                   ModelMap modelMap) {
        filter.setSelectedTypes(new ArrayList<UserType>());
        return filterByTypes(filter, model, modelMap);
    }

    @RequestMapping("detail")
    public String detail(@RequestParam("id") Long id, Model model) {
        model.addAttribute("user", utilisateurService.getById(id));
        return "utilisateur/details";
    }

    @RequestMapping("creer")
    public String creer(@ModelAttribute("newuser") Utilisateur utilisateur) {
        return "utilisateur/create";
    }

    @RequestMapping("/update/{user}")
    public String update(@ModelAttribute("user") Utilisateur utilisateur) {
        // retyping password required
        utilisateur.setPasswd("");
        return "utilisateur/update";
    }

    @RequestMapping("ajouterUtilisateur")
    public String ajouterUtilisateur(@Valid @ModelAttribute("newuser") Utilisateur utilisateur,
                                     BindingResult bindingResult,
                                     @ModelAttribute("filter") UtilisateurFilter filter,
                                     Model model,
                                     ModelMap modelMap) {
        if (bindingResult.hasErrors()) {
            return "utilisateur/create";
        }
        populate(utilisateur);
        utilisateurService.create(utilisateur);
        return filterByTypes(filter, model, modelMap);
    }

    @RequestMapping("/update/miseajour")
    public String updateUtilisateur(@Valid @ModelAttribute("user") Utilisateur utilisateur,
                                    BindingResult bindingResult,
                                    @ModelAttribute("filter") UtilisateurFilter filter,
                                    Model model,
                                    ModelMap modelMap) {
        if (bindingResult.hasErrors()) {
            return "utilisateur/update";
        }
        populate(utilisateur);
        utilisateurService.update(utilisateur);
        return filterByTypes(filter, model, modelMap);
    }

    @RequestMapping("/delete")
    public String deleteUtilisateur(@RequestParam("id") Long id,
                                    @ModelAttribute("filter") UtilisateurFilter filter,
                                    Model model,
                                    ModelMap modelMap) {
        utilisateurService.delete(id);
        return filterByTypes(filter, model, modelMap);
    }

    // OUTILS

    private void populate(Utilisateur utilisateur) {
        utilisateur.setPasswd(bCryptPasswordEncoder.encode(utilisateur.getPasswd()));
        utilisateur.setRoles(Arrays.asList(
                roleService.getRoleByName(Role.Roles.USER.toString())));
        utilisateur.setEnabled(true);
    }

    private Utilisateur getCurrentUser(Principal principal) {
        return utilisateurService.getByUsername(principal.getName());
    }

    @RequestMapping("search")
    public String search(@ModelAttribute("searchText") String searchText, Model model) throws Exception {
        model.addAttribute("usersRecherche", utilisateurService.search(searchText));
        return "utilisateur/search";
    }

  /*
    @RequestMapping("search")
    public String search(@RequestParam(required= false, defaultValue="") String username, Model model)
    {
        model.addAttribute("usersRecherche", utilisateurService.search(username.trim()));
        return "utilisateur/all";
    }
    */
}

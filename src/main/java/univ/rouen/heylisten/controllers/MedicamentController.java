package univ.rouen.heylisten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import univ.rouen.heylisten.ejb.Medicament;
import univ.rouen.heylisten.services.MedicamentService;
import univ.rouen.heylisten.validator.MedicamentValidator;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/medicament")
@SessionAttributes({"medicaments", "medoc"})
public class MedicamentController {
    @Autowired
    private MedicamentService medicamentService;
    @Autowired
    private MedicamentValidator medicamentValidator;

    @InitBinder({"newmedoc", "medoc"})
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(medicamentValidator);
    }

    @ModelAttribute("medicaments")
    public List<Medicament> populateModelWithMedicaments() {
        return medicamentService.getAllOrderedByLibelle();
    }

    @RequestMapping("all")
    public String allMedicaments(Model model) {
        model.addAttribute("medicaments", medicamentService.getAllOrderedByLibelle());
        return "medicament/all";
    }

    @RequestMapping("detail")
    public String detail(@RequestParam("id") Long id, Model model) {
        model.addAttribute("medoc", medicamentService.getById(id));
        return "medicament/details";
    }

    @RequestMapping("creer")
    public String creer(@ModelAttribute("newmedoc") Medicament medicament) {
        return "medicament/create";
    }

    @RequestMapping("/update/{medoc}")
    public String update(@ModelAttribute("medoc") Medicament medicament) {
        return "medicament/update";
    }

    @RequestMapping("ajouterMedicament")
    public String ajouterMedicament(@Valid @ModelAttribute("newmedoc") Medicament medicament,
                                     BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "medicament/create";
        }
        medicamentService.create(medicament);
        return "redirect:/medicament/all";
    }

    @RequestMapping("/update/miseajour")
    public String updateMedicament(@Valid @ModelAttribute("medoc") Medicament medicament,
                                    BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "medicament/update";
        }
        medicamentService.update(medicament);
        return "redirect:/medicament/all";
    }

    @RequestMapping("/delete")
    public String deleteUtilisateur(@RequestParam("id") Long id) {
        medicamentService.delete(id);
        return "redirect:/medicament/all";
    }

    @RequestMapping("search")
    public String search(@ModelAttribute("searchText") String searchText, Model model) throws Exception {
        model.addAttribute("medicamentsRecherche", medicamentService.search(searchText));
        return "medicament/search";
    }

  /*
    @RequestMapping("search")
    public String search(@RequestParam(required= false, defaultValue="") String libelle, Model model)
    {
        model.addAttribute("medicamentsRecherche", medicamentService.search(libelle.trim()));
        return "medicament/all";
    }
    */
}

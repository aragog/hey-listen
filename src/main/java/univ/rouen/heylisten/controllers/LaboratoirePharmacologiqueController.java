package univ.rouen.heylisten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import univ.rouen.heylisten.ejb.LaboratoirePharmacologique;
import univ.rouen.heylisten.exceptions.HasParentsException;
import univ.rouen.heylisten.services.LaboratoirePharmacologiqueService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/laboratoirePharmacologique")
@SessionAttributes({"laboratoiresPharmacologiques", "laboratoirePharmacologique"})
public class LaboratoirePharmacologiqueController {
    @Autowired
    private LaboratoirePharmacologiqueService laboratoirePharmacologiqueService;

    @ModelAttribute("laboratoiresPharmacologiques")
    public List<LaboratoirePharmacologique> populateModelWithLaboratoiresPharmacologiques() {
        return laboratoirePharmacologiqueService.getAllOrderedByLibelle();
    }

    @RequestMapping("all")
    public String all(Model model) {
        model.addAttribute("laboratoiresPharmacologiques", laboratoirePharmacologiqueService.getAllOrderedByLibelle());
        return "laboratoirePharmacologique/all";
    }

    @RequestMapping("detail")
    public String detail(@RequestParam("id") Long id, Model model) {
        model.addAttribute("laboratoirePharmacologique", laboratoirePharmacologiqueService.getById(id));
        return "laboratoirePharmacologique/details";
    }

    @RequestMapping("creer")
    public String creer(@ModelAttribute("newlaboratoirePharmacologique") LaboratoirePharmacologique laboratoirePharmacologique) {
        return "laboratoirePharmacologique/create";
    }

    @RequestMapping("/update/{laboratoirePharmacologique}")
    public String update(@ModelAttribute("laboratoirePharmacologique") LaboratoirePharmacologique laboratoirePharmacologique) {
        return "laboratoirePharmacologique/update";
    }

    @RequestMapping("ajouter")
    public String ajouter(@Valid @ModelAttribute("newlaboratoirePharmacologique") LaboratoirePharmacologique laboratoirePharmacologique,
                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "laboratoirePharmacologique/create";
        }
        laboratoirePharmacologiqueService.create(laboratoirePharmacologique);
        return "redirect:/laboratoirePharmacologique/all";
    }

    @RequestMapping("/update/miseajour")
    public String update(@Valid @ModelAttribute("laboratoirePharmacologique") LaboratoirePharmacologique laboratoirePharmacologique,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "laboratoirePharmacologique/update";
        }
        laboratoirePharmacologiqueService.update(laboratoirePharmacologique);
        return "redirect:/laboratoirePharmacologique/all";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam("id") Long id, RedirectAttributes redirectAttrs) {
        if (laboratoirePharmacologiqueService.hasAnyParents(id)) {
            redirectAttrs.addFlashAttribute("error",
                    new HasParentsException("Ce laboratoire est encore attach&eacute;"
                            + " &agrave; un dispositif m&eacute;dical"));
        } else {
            laboratoirePharmacologiqueService.delete(id);
        }
        return "redirect:/laboratoirePharmacologique/all";
    }

    @RequestMapping("search")
    public String search(@ModelAttribute("searchText") String searchText, Model model) throws Exception {
        model.addAttribute("laboratoiresPharmacologiquesRecherche", laboratoirePharmacologiqueService.search(searchText));
        return "laboratoirePharmacologique/search";
    }

    /*
    @RequestMapping("search")
    public String search(@RequestParam(required= false, defaultValue="") String libelle, Model model)
    {
        model.addAttribute("laboratoiresPharmacologiquesRecherche", laboratoirePharmacologiqueService.search(libelle.trim()));
        return "laboratoirePharmacologique/all";
    }
    */
}

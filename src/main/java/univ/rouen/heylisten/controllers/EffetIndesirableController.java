package univ.rouen.heylisten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import univ.rouen.heylisten.ejb.EffetIndesirable;
import univ.rouen.heylisten.services.EffetIndesirableService;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/effetindesirable")
@SessionAttributes({"effetsIndesirables", "effetIndesirable"})
public class EffetIndesirableController {
    @Autowired
    private EffetIndesirableService effetIndesirableService;

    @ModelAttribute("effetsIndesirables")
    public List<EffetIndesirable> populateModelWithEffetsIndesirables() {
        return effetIndesirableService.getAll();
    }

    @RequestMapping("all")
    public String allEffetsIndesirables(Model model) {
        model.addAttribute("effetsIndesirables", effetIndesirableService.getAll());
        return "effetIndesirable/all";
    }

    @RequestMapping("detail")
    public String detail(@RequestParam Long id, Model model) {
        model.addAttribute("effetIndesirable", effetIndesirableService.getById(id));
        return "effetIndesirable/details";
    }

    @RequestMapping("creer")
    public String creer(@ModelAttribute("newEI")EffetIndesirable effetIndesirable) {
        return "effetIndesirable/create";
    }

    @RequestMapping("ajouterEffetIndesirable")
    public String ajouterEffetIndesirable(@Valid @ModelAttribute("newEI") EffetIndesirable effetIndesirable,
                                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "effetIndesirable/create";
        }
        effetIndesirable.setPremiereDeclaration(new Date());
        effetIndesirable.setDerniereDeclaration(new Date());
        effetIndesirableService.create(effetIndesirable);
        return "redirect:/effetindesirable/all";
    }

    @RequestMapping("/update/{effetIndesirable}")
    public String update(@ModelAttribute("effetIndesirable") EffetIndesirable effetIndesirable) {
        return "effetIndesirable/update";
    }

    @RequestMapping("/update/miseajour")
    public String update(@Valid @ModelAttribute("effetIndesirable") EffetIndesirable effetIndesirable,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "effetIndesirable/update";
        }
        effetIndesirableService.update(effetIndesirable);
        return "redirect:/effetindesirable/all";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam("id") Long id) {
        effetIndesirableService.delete(id);
        return "redirect:/effetindesirable/all";
    }
}

package univ.rouen.heylisten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import univ.rouen.heylisten.ejb.DispositifMedical;
import univ.rouen.heylisten.ejb.LaboratoirePharmacologique;
import univ.rouen.heylisten.services.DispositifMedicalService;
import univ.rouen.heylisten.services.LaboratoirePharmacologiqueService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/dispositifMedical")
@SessionAttributes({"dispositifsMedicaux", "dispositifMedical"})
public class DispositifMedicalController {
    @Autowired
    private DispositifMedicalService dispositifMedicalService;
    @Autowired
    private LaboratoirePharmacologiqueService laboratoirePharmacologiqueService;

    @ModelAttribute("dispositifsMedicaux")
    public List<DispositifMedical> populateModelWithDispositifsMedicaux() {
        return dispositifMedicalService.getAllOrderedByLibelle();
    }

    @ModelAttribute("laboratoires")
    public List<LaboratoirePharmacologique> populateModelWithLaboratoires() {
        return laboratoirePharmacologiqueService.getAllOrderedByLibelle();
    }

    @RequestMapping("all")
    public String all(Model model) {
        model.addAttribute("dispositifsMedicaux", dispositifMedicalService.getAllOrderedByLibelle());
        return "dispositifMedical/all";
    }

    @RequestMapping("detail")
    public String detail(@RequestParam("id") Long id, Model model) {
        model.addAttribute("dispositifMedical", dispositifMedicalService.getById(id));
        return "dispositifMedical/details";
    }

    @RequestMapping("creer")
    public String creer(@ModelAttribute("newdispositifMedical") DispositifMedical dispositifMedical) {
        return "dispositifMedical/create";
    }

    @RequestMapping("/update/{dispositifMedical}")
    public String update(@ModelAttribute("dispositifMedical") DispositifMedical dispositifMedical) {
        return "dispositifMedical/update";
    }

    @RequestMapping("ajouter")
    public String ajouter(@Valid @ModelAttribute("newdispositifMedical") DispositifMedical dispositifMedical,
                                    BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "dispositifMedical/create";
        }
        dispositifMedicalService.create(dispositifMedical);
        return "redirect:/dispositifMedical/all";
    }

    @RequestMapping("/update/miseajour")
    public String update(@Valid @ModelAttribute("dispositifMedical") DispositifMedical dispositifMedical,
                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "dispositifMedical/update";
        }
        dispositifMedicalService.update(dispositifMedical);
        return "redirect:/dispositifMedical/all";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam("id") Long id) {
        dispositifMedicalService.delete(id);
        return "redirect:/dispositifMedical/all";
    }

    @RequestMapping("search")
    public String search(@ModelAttribute("searchText") String searchText, Model model) throws Exception {
        model.addAttribute("dispositifsMedicauxRecherche", dispositifMedicalService.search(searchText));
        return "dispositifMedical/search";
    }
/*
    @RequestMapping("search")
    public String search(@RequestParam(required= false, defaultValue="") String libelle, Model model)
    {
        model.addAttribute("dispositifsMedicauxRecherche", dispositifMedicalService.search(libelle.trim()));
        return "dispositifMedical/all";
    }
    */
}

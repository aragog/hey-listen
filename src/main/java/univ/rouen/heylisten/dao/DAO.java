package univ.rouen.heylisten.dao;

import java.io.Serializable;
import java.util.List;


/**
 * Contrat d'un DAO pour les opérations basiques du CRUD.
 * 
 * @inv
 *     0 <= getAll().size()
 *     getAll().size() == nombre d'entités de ce type persistées
 * 
 * @param <T> Entité à gérer.
 * @param <I> Identifiant de l'entité.
 */
public interface DAO<T, I extends Serializable> {
    /**
     * Retourne la ressource d'identifiant <code>id</code>.
     * @pre
     *   id != null
     */
    T getById(I id);
    /**
     * Retourne toutes les ressources.
     */
    List<T> getAll();
    /**
     * Persiste la ressource <code>resource</code>.
     * @pre
     *     resource != null
     * @post
     *     getAll().size() == old getAll().size() + 1
     */
    Serializable create(T resource);
    /**
     * Met à jour la ressource <code>resource</code>.
     * @pre
     *     resource != null
     */
    void update(T resource);
    /**
     * Supprime la ressource <code>resource</code>.
     * @pre
     *     resource != null
     * @post
     *     getAll().size() == old getAll().size() - 1
     */
    void delete(I id);
}

package univ.rouen.heylisten.dao;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.LaboratoirePharmacologique;

import java.util.List;

@Repository
public class LaboratoirePharmacologiqueDAO extends SimpleEntityManager<LaboratoirePharmacologique, Long> {
    public LaboratoirePharmacologiqueDAO() {
        this(LaboratoirePharmacologique.class);
    }

    public LaboratoirePharmacologiqueDAO(Class<LaboratoirePharmacologique> entityClass) {
        super(entityClass);
    }

    public boolean hasAnyParents(Long id) {
        final LaboratoirePharmacologique laboratoirePharmacologique = getById(id);
        Hibernate.initialize(laboratoirePharmacologique.getDispositifs());
        return !laboratoirePharmacologique.getDispositifs().isEmpty();
    }

    public List<LaboratoirePharmacologique> getAllOrderedByLibelle() {
        return getSession().createCriteria(LaboratoirePharmacologique.class)
                .addOrder(Order.desc("libelle"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public void index() throws Exception
    {
        try
        {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    public List<LaboratoirePharmacologique> search(String searchText) throws Exception {
        try {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();

            QueryBuilder qb = fullTextSession.getSearchFactory()
                    .buildQueryBuilder().forEntity(LaboratoirePharmacologique.class).get();
            org.apache.lucene.search.Query query = qb
                    .keyword()
                    .wildcard()
                    .onField("libelle")
                    .matching(searchText)
                    .createQuery();

            org.hibernate.Query hibQuery =
                    fullTextSession.createFullTextQuery(query, LaboratoirePharmacologique.class);

            List<LaboratoirePharmacologique> results = hibQuery.list();
            return results;
        }catch (Exception e){
            throw e;
        }
    }

    /*
    public List<LaboratoirePharmacologique> search(String libelle){
        return getSession().createCriteria(LaboratoirePharmacologique.class)
                .add(Restrictions.ilike("libelle", libelle + "%"))
                .list();
    }
    */
}

package univ.rouen.heylisten.dao;

import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.CasClasseChimiqueEI;

@Repository

public class CasClasseChimiqueEIDAO extends SimpleEntityManager<CasClasseChimiqueEI, Long> {
    public CasClasseChimiqueEIDAO() {
        this(CasClasseChimiqueEI.class);
    }

    public CasClasseChimiqueEIDAO(Class<CasClasseChimiqueEI> entityClass) {
        super(entityClass);
    }
}

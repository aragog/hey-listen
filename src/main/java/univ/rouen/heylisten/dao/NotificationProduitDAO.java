package univ.rouen.heylisten.dao;

import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.NotificationProduit;

/**
 * Created by IuliaCristina on 12/18/2014.
 */

@Repository
public class NotificationProduitDAO extends SimpleEntityManager<NotificationProduit, Long> {
    public NotificationProduitDAO() { this(NotificationProduit.class); }

    public NotificationProduitDAO(Class<NotificationProduit> entityClass) {
        super(entityClass);
    }
}

package univ.rouen.heylisten.dao;

import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.CompositionMedicament;

@Repository
public class CompositionMedicamentDAO extends SimpleEntityManager<CompositionMedicament, Long> {
    public CompositionMedicamentDAO() {
        this(CompositionMedicament.class);
    }

    public CompositionMedicamentDAO(Class<CompositionMedicament> entityClass) {
        super(entityClass);
    }
}

package univ.rouen.heylisten.dao;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.Role;

import java.util.List;

@Repository
public class RoleDAO extends SimpleEntityManager<Role, Long> {
    public RoleDAO() { this(Role.class); }

    public RoleDAO(Class<Role> entityClass) {
        super(entityClass);
    }

    public Role getRoleByName(String name) {
        return (Role) getSession().createCriteria(Role.class)
                .add(Restrictions.eq("role", name))
                .uniqueResult();
    }

    public List<Role> search(String role){
        return getSession().createCriteria(Role.class)
                .add(Restrictions.ilike("role", role + "%"))
                .list();
    }
}

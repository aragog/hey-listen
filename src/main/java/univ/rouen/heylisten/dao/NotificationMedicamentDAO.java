package univ.rouen.heylisten.dao;

import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.NotificationMedicament;

/**
 * Created by IuliaCristina on 12/18/2014.
 */

@Repository
public class NotificationMedicamentDAO extends SimpleEntityManager<NotificationMedicament, Long> {
    public NotificationMedicamentDAO() { this(NotificationMedicament.class); }

    public NotificationMedicamentDAO(Class<NotificationMedicament> entityClass) {
        super(entityClass);
    }
}

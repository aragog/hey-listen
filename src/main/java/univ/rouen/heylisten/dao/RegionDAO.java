package univ.rouen.heylisten.dao;

import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.Region;

@Repository
public class RegionDAO extends SimpleEntityManager<Region, Long> {
    public RegionDAO() { this(Region.class); }

    public RegionDAO(Class<Region> entityClass) {
        super(entityClass);
    }
}

package univ.rouen.heylisten.dao;

import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.HelloBean;

@Repository
public class HelloBeanDAO extends SimpleEntityManager<HelloBean, Long> {
    public HelloBeanDAO() {
        this(HelloBean.class);
    }
    
    public HelloBeanDAO(Class<HelloBean> entityClass) {
        super(entityClass);
    }
}

package univ.rouen.heylisten.dao;

import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.Pays;

/**
 * Created by IuliaCristina on 12/18/2014.
 */

@Repository
public class PaysDAO extends SimpleEntityManager<Pays, Long> {
    public PaysDAO() { this(Pays.class); }

    public PaysDAO(Class<Pays> entityClass) {
        super(entityClass);
    }
}

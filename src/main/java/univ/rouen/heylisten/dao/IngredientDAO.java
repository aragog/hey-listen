package univ.rouen.heylisten.dao;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.Ingredient;

import java.util.List;

@Repository
public class IngredientDAO extends SimpleEntityManager<Ingredient, Long> {
    public IngredientDAO() {
        this(Ingredient.class);
    }

    public IngredientDAO(Class<Ingredient> entityClass) {
        super(entityClass);
    }

    public boolean hasAnyParents(Long id) {
        final Ingredient ingredient = getById(id);
        Hibernate.initialize(ingredient.getProduits());
        return !ingredient.getProduits().isEmpty();
    }

    public List<Ingredient> getAllOrderedByLibelle() {
        return getSession().createCriteria(Ingredient.class)
                .addOrder(Order.desc("libelle"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public void index() throws Exception
    {
        try
        {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    public List<Ingredient> search(String searchText) throws Exception {
        try {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();

            QueryBuilder qb = fullTextSession.getSearchFactory()
                    .buildQueryBuilder().forEntity(Ingredient.class).get();
            org.apache.lucene.search.Query query = qb
                    .keyword()
                    .wildcard()
                    .onField("libelle")
                    .matching(searchText)
                    .createQuery();

            org.hibernate.Query hibQuery =
                    fullTextSession.createFullTextQuery(query, Ingredient.class);

            List<Ingredient> results = hibQuery.list();
            return results;
        }catch (Exception e){
            throw e;
        }
    }


  /*
    public List<Ingredient> search(String libelle){
        return getSession().createCriteria(Ingredient.class)
                .add(Restrictions.ilike("libelle", libelle + "%"))
                .list();
    }
    */
}

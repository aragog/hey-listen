package univ.rouen.heylisten.dao;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.SubstanceActive;

import java.util.List;

@Repository
public class SubstanceActiveDAO extends SimpleEntityManager<SubstanceActive, Long> {
    public SubstanceActiveDAO() {
        this(SubstanceActive.class);
    }

    public SubstanceActiveDAO(Class<SubstanceActive> entityClass) {
        super(entityClass);
    }

    public boolean hasAnyParents(Long id) {
        final SubstanceActive substanceActive = getById(id);
        Hibernate.initialize(substanceActive.getCasSubstanceEIs());
        Hibernate.initialize(substanceActive.getClasseChimiques());
        Hibernate.initialize(substanceActive.getClassePharmacologiques());
        Hibernate.initialize(substanceActive.getCompositionMedicaments());
        return !substanceActive.getCasSubstanceEIs().isEmpty()
                || !substanceActive.getClasseChimiques().isEmpty()
                || !substanceActive.getClassePharmacologiques().isEmpty()
                || !substanceActive.getCompositionMedicaments().isEmpty();
    }

    public List<SubstanceActive> getAllOrderedByLibelle() {
        return getSession().createCriteria(SubstanceActive.class)
                .addOrder(Order.desc("libelle"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public void index() throws Exception
    {
        try
        {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    public List<SubstanceActive> search(String searchText) throws Exception {
        try {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();

            QueryBuilder qb = fullTextSession.getSearchFactory()
                    .buildQueryBuilder().forEntity(SubstanceActive.class).get();
            org.apache.lucene.search.Query query = qb
                    .keyword()
                    .wildcard()
                    .onField("libelle")
                    .matching(searchText)
                    .createQuery();

            org.hibernate.Query hibQuery =
                    fullTextSession.createFullTextQuery(query, SubstanceActive.class);

            List<SubstanceActive> results = hibQuery.list();
            return results;
        }catch (Exception e){
            throw e;
        }
    }

/*
    public List<SubstanceActive> search(String libelle){
        return getSession().createCriteria(SubstanceActive.class)
                .add(Restrictions.ilike("libelle", libelle + "%"))
                .list();
    }
    */
}

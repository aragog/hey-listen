package univ.rouen.heylisten.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.DispositifMedical;

import java.util.List;

@Repository
public class DispositifMedicalDAO extends SimpleEntityManager<DispositifMedical, Long> {
    public DispositifMedicalDAO() {
        this(DispositifMedical.class);
    }

    public DispositifMedicalDAO(Class<DispositifMedical> entityClass) {
        super(entityClass);
    }

    public List<DispositifMedical> getAllOrderedByLibelle() {
        return getSession().createCriteria(DispositifMedical.class)
                .addOrder(Order.desc("libelle"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public void index() throws Exception
    {
        try
        {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    public List<DispositifMedical> search(String searchText) throws Exception {
        try {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();

            QueryBuilder qb = fullTextSession.getSearchFactory()
                    .buildQueryBuilder().forEntity(DispositifMedical.class).get();
            org.apache.lucene.search.Query query = qb
                    .keyword()
                    .wildcard()
                    .onField("libelle")
                    .matching(searchText)
                    .createQuery();

            org.hibernate.Query hibQuery =
                    fullTextSession.createFullTextQuery(query, DispositifMedical.class);

            List<DispositifMedical> results = hibQuery.list();
            return results;
        }catch (Exception e){
            throw e;
        }
    }

    /*
    public List<DispositifMedical> search(String libelle){
        return getSession().createCriteria(DispositifMedical.class)
                .add(Restrictions.ilike("libelle", libelle + "%"))
                .list();
    }
    */
}

package univ.rouen.heylisten.dao;

import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.CasClassePharmacologiqueEI;

@Repository
public class CasClassePharmacologiqueEIDAO extends SimpleEntityManager<CasClassePharmacologiqueEI, Long> {
    public CasClassePharmacologiqueEIDAO() {
        this(CasClassePharmacologiqueEI.class);
    }

    public CasClassePharmacologiqueEIDAO(Class<CasClassePharmacologiqueEI> entityClass) {
        super(entityClass);
    }
}

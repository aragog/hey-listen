package univ.rouen.heylisten.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public class SimpleEntityManager<T, I extends Serializable> implements DAO<T, I> {
    @Autowired
    private SessionFactory sessionFactory;
    private Class<T> entityClass;
    
    public SimpleEntityManager() {}
    
    /**
     * Gestionnaire d'entités de type <code>entityClass</code>.
     * @pre
     *     entityClass != null
     */
    public SimpleEntityManager(Class<T> entityClass) {
        if (entityClass == null) {
            throw new IllegalArgumentException();
        }
        this.entityClass = entityClass;
    }

    @SuppressWarnings("unchecked")
    public T getById(I id) {
        if (id == null) {
            throw new IllegalArgumentException();
        }
        return (T) getSession().get(entityClass, id);
    }

    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        return getSession()
                .createCriteria(entityClass)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public Serializable create(T resource) {
        if (resource == null) {
            throw new IllegalArgumentException();
        }
        return getSession().save(resource);
    }

    public void update(T resource) {
        if (resource == null) {
            throw new IllegalArgumentException();
        }
        getSession().update(resource);
    }

    public void delete(I id) {
        if (id == null) {
            throw new IllegalArgumentException();
        }
        getSession().delete(getSession().get(entityClass, id));
    }
    
    /*
     * Retourne la session courante.
     */
    public Session getSession() {
        return sessionFactory.getCurrentSession();
    }

}

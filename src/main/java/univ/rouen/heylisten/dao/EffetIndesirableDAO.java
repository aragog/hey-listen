package univ.rouen.heylisten.dao;

import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.EffetIndesirable;

@Repository
public class EffetIndesirableDAO extends SimpleEntityManager<EffetIndesirable, Long> {
    public EffetIndesirableDAO() {
        this(EffetIndesirable.class);
    }

    public EffetIndesirableDAO(Class<EffetIndesirable> entityClass) {
        super(entityClass);
    }

}

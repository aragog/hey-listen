package univ.rouen.heylisten.dao;

import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.NotificationDispositif;

/**
 * Created by IuliaCristina on 12/18/2014.
 */
@Repository
public class NotificationDispositifDAO extends SimpleEntityManager<NotificationDispositif, Long> {
    public NotificationDispositifDAO() { this(NotificationDispositif.class); }

    public NotificationDispositifDAO(Class<NotificationDispositif> entityClass) {
        super(entityClass);
    }
}

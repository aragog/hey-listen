package univ.rouen.heylisten.dao;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.Medicament;

import java.util.List;

@Repository
public class MedicamentDAO extends SimpleEntityManager<Medicament, Long> {
    public MedicamentDAO() {
        this(Medicament.class);
    }

    public MedicamentDAO(Class<Medicament> entityClass) {
        super(entityClass);
    }

    public boolean hasAnyParents(Long id) {
        final Medicament medicament = getById(id);
        Hibernate.initialize(medicament.getCompositionMedicaments());
        Hibernate.initialize(medicament.getNotificationMedicaments());
        return !medicament.getCompositionMedicaments().isEmpty()
                || ! medicament.getNotificationMedicaments().isEmpty();
    }

    public List<Medicament> getAllOrderedByLibelle() {
        return getSession().createCriteria(Medicament.class)
                .addOrder(Order.desc("libelle"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public Medicament getByCodeCis(String codeCis) {
        return (Medicament) getSession().createCriteria(Medicament.class)
                .add(Restrictions.eq("code_cis", codeCis))
                .uniqueResult();
    }

    public void index() throws Exception
    {
        try
        {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    public List<Medicament> search(String searchText) throws Exception {
        try {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();

            QueryBuilder qb = fullTextSession.getSearchFactory()
                    .buildQueryBuilder().forEntity(Medicament.class).get();
            org.apache.lucene.search.Query query = qb
                    .keyword()
                    .wildcard()
                    .onField("code_cis")
                    .andField("libelle")
                    .matching(searchText)
                    .createQuery();

            org.hibernate.Query hibQuery =
                    fullTextSession.createFullTextQuery(query, Medicament.class);

            List<Medicament> results = hibQuery.list();
            return results;
        }catch (Exception e){
            throw e;
        }
    }

    /*
    public List<Medicament> search(String libelle){
        return getSession().createCriteria(Medicament.class)
                .add(Restrictions.ilike("libelle", libelle + "%"))
                .list();
    }
    */
}

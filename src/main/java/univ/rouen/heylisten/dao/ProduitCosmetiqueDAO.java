package univ.rouen.heylisten.dao;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.ProduitCosmetique;

import java.util.List;

@Repository
public class ProduitCosmetiqueDAO extends SimpleEntityManager<ProduitCosmetique, Long> {
    public ProduitCosmetiqueDAO() {
        this(ProduitCosmetique.class);
    }

    public ProduitCosmetiqueDAO(Class<ProduitCosmetique> entityClass) {
        super(entityClass);
    }

    public ProduitCosmetique getByIdFull(Long id) {
        final ProduitCosmetique produitCosmetique = getById(id);
        Hibernate.initialize(produitCosmetique.getIngredients());
        return produitCosmetique;
    }

    public List<ProduitCosmetique> getAllOrderedByLibelle() {
        return getSession().createCriteria(ProduitCosmetique.class)
                .addOrder(Order.desc("libelle"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public void index() throws Exception
    {
        try
        {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    public List<ProduitCosmetique> search(String searchText) throws Exception {
        try {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();

            QueryBuilder qb = fullTextSession.getSearchFactory()
                    .buildQueryBuilder().forEntity(ProduitCosmetique.class).get();
            org.apache.lucene.search.Query query = qb
                    .keyword()
                    .wildcard()
                    .onField("libelle")
                    .matching(searchText)
                    .createQuery();

            org.hibernate.Query hibQuery =
                    fullTextSession.createFullTextQuery(query, ProduitCosmetique.class);

            List<ProduitCosmetique> results = hibQuery.list();
            return results;
        }catch (Exception e){
            throw e;
        }
    }

  /*
    public List<ProduitCosmetique> search(String libelle){
        return getSession().createCriteria(ProduitCosmetique.class)
                .add(Restrictions.ilike("libelle", libelle + "%"))
                .list();
    }
    */
}

package univ.rouen.heylisten.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.Utilisateur;
import univ.rouen.heylisten.util.UserType;

import java.util.LinkedList;
import java.util.List;

@Repository
public class UtilisateurDAO extends SimpleEntityManager<Utilisateur, Long> {
    public UtilisateurDAO() {
        this(Utilisateur.class);
    }

    public UtilisateurDAO(Class<Utilisateur> entityClass) {
        super(entityClass);
    }

    public Utilisateur getByUsername(String username) {
        return (Utilisateur) getSession().createCriteria(Utilisateur.class)
                .add(Restrictions.eq("username", username))
                .uniqueResult();
    }

    public List<Utilisateur> getAllOrderedByUsername() {
        return getSession().createCriteria(Utilisateur.class)
                .addOrder(Order.desc("username"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public List<Utilisateur> getAllByRegionAndUserTypes(List<UserType> types, UserType userType, Long regionId) {
        final Criteria c = getSession().createCriteria(Utilisateur.class);
        List<Criterion> criterions = new LinkedList<>();
        for (UserType type : types) {
            criterions.add(Restrictions.eq("type", type));
        }
        c.add(Restrictions.or(criterions.toArray(new Criterion[criterions.size()])));
        if (!userType.equals(UserType.ADMIN) && !userType.equals(UserType.CNPV)) {
            c.add(Restrictions.eq("region.idRegion", regionId));
        }
        return c.addOrder(Order.desc("username"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public List<Utilisateur> getAllByRegionAndUserType(Long regionId, UserType userType) {
        final Criteria criteria = getSession().createCriteria(Utilisateur.class);
        if (!userType.equals(UserType.ADMIN) && !userType.equals(UserType.CNPV)) {
            criteria.add(Restrictions.eq("region.idRegion", regionId));
        }

        return criteria.add(Restrictions.lt("type", userType))
                .addOrder(Order.desc("username"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public void index() throws Exception
    {
        try
        {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    public List<Utilisateur> search(String searchText) throws Exception {
        try {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();

            QueryBuilder qb = fullTextSession.getSearchFactory()
                    .buildQueryBuilder().forEntity(Utilisateur.class).get();
            org.apache.lucene.search.Query query = qb
                    .keyword()
                    .wildcard()
                    .onField("username")
                    .matching(searchText)
                    .createQuery();

            org.hibernate.Query hibQuery =
                    fullTextSession.createFullTextQuery(query, Utilisateur.class);

            List<Utilisateur> results = hibQuery.list();
            return results;
        }catch (Exception e){
            throw e;
        }
    }

/*
    public List<Utilisateur> search(String username){
        return getSession().createCriteria(Utilisateur.class)
                .add(Restrictions.ilike("username", username + "%"))
                .list();
    }
    */
}

package univ.rouen.heylisten.dao;

import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.CasSubstanceEI;

/**
 * Created by IuliaCristina on 12/18/2014.
 */
@Repository
public class CasSubstanceEIDAO extends SimpleEntityManager<CasSubstanceEI, Long> {
    public CasSubstanceEIDAO() {
        this(CasSubstanceEI.class);
    }

    public CasSubstanceEIDAO(Class<CasSubstanceEI> entityClass) {
        super(entityClass);
    }
}

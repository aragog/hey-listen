package univ.rouen.heylisten.dao;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.ClassePharmacologique;

import java.util.List;

@Repository
public class ClassePharmacologiqueDAO extends SimpleEntityManager<ClassePharmacologique, Long> {

    public ClassePharmacologiqueDAO() {
        this(ClassePharmacologique.class);
    }

    public ClassePharmacologiqueDAO(Class<ClassePharmacologique> entityClass) {
        super(entityClass);
    }

    public List<ClassePharmacologique> getAllOrderedById() {
        return getSession().createCriteria(ClassePharmacologique.class)
                .addOrder(Order.asc("idClassePharmacologique"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public ClassePharmacologique getByIdClassePharmacologique(String idClassePharmacologique) {
        return (ClassePharmacologique) getSession().createCriteria(ClassePharmacologique.class)
                .add(Restrictions.eq("idClassePharmacologique", idClassePharmacologique))
                .uniqueResult();
    }

    public ClassePharmacologique getByLibelle(String libelle) {
        return (ClassePharmacologique) getSession().createCriteria(ClassePharmacologique.class)
                .add(Restrictions.eq("libelle", libelle))
                .uniqueResult();
    }

    public boolean hasAnyParents(Long id) {
        final ClassePharmacologique classePharmacologique = getById(id);
        Hibernate.initialize(classePharmacologique.getClassePharmacologiqueFils());
        return !classePharmacologique.getClassePharmacologiqueFils().isEmpty();
    }

    public void index() throws Exception
    {
        try
        {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    public List<ClassePharmacologique> search(String searchText) throws Exception {
        try {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();

            QueryBuilder qb = fullTextSession.getSearchFactory()
                    .buildQueryBuilder().forEntity(ClassePharmacologique.class).get();
            org.apache.lucene.search.Query query = qb
                    .keyword()
                    .wildcard()
                    .onField("idClassePharmacologique")
                    .andField("libelle")
                    .matching(searchText)
                    .createQuery();

            org.hibernate.Query hibQuery =
                    fullTextSession.createFullTextQuery(query, ClassePharmacologique.class);

            List<ClassePharmacologique> results = hibQuery.list();
            return results;
        }catch (Exception e){
            throw e;
        }
    }

/*
    public List<ClassePharmacologique> search(String libelle){
        return getSession().createCriteria(ClassePharmacologique.class)
                .add(Restrictions.ilike("libelle", libelle + "%"))
                .list();
    }
    */
}

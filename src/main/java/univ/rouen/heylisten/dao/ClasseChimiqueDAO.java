package univ.rouen.heylisten.dao;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;
import univ.rouen.heylisten.ejb.ClasseChimique;
import univ.rouen.heylisten.ejb.ClassePharmacologique;

import java.util.List;

@Repository
public class ClasseChimiqueDAO extends SimpleEntityManager<ClasseChimique, Long> {

    public ClasseChimiqueDAO() {
        this(ClasseChimique.class);
    }

    public ClasseChimiqueDAO(Class<ClasseChimique> entityClass) {
        super(entityClass);
    }

    public List<ClasseChimique> getAllOrderedById() {
        return getSession().createCriteria(ClasseChimique.class)
                .addOrder(Order.asc("idClasseChimique"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public ClasseChimique getByIdClasseChimique(String idClasseChimique) {
        return (ClasseChimique) getSession().createCriteria(ClasseChimique.class)
                .add(Restrictions.eq("idClasseChimique", idClasseChimique))
                .uniqueResult();
    }

    public ClasseChimique getByLibelle(String libelle) {
        return (ClasseChimique) getSession().createCriteria(ClasseChimique.class)
                .add(Restrictions.eq("libelle", libelle))
                .uniqueResult();
    }

    public boolean hasAnyParents(Long id) {
        final ClasseChimique classeChimique = getById(id);
        Hibernate.initialize(classeChimique.getClasseChimiqueFils());
        return !classeChimique.getClasseChimiqueFils().isEmpty();
    }

    public void index() throws Exception
    {
        try
        {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    public List<ClasseChimique> search(String searchText) throws Exception {
        try {
            Session session = getSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();

            QueryBuilder qb = fullTextSession.getSearchFactory()
                    .buildQueryBuilder().forEntity(ClasseChimique.class).get();
            org.apache.lucene.search.Query query = qb
                    .keyword()
                    .wildcard()
                    .onField("idClasseChimique")
                    .andField("libelle")
                    .matching(searchText)
                    .createQuery();

            org.hibernate.Query hibQuery =
                    fullTextSession.createFullTextQuery(query, ClasseChimique.class);

            List<ClasseChimique> results = hibQuery.list();
            return results;
        }catch (Exception e){
            throw e;
        }
    }

/*
    public List<ClasseChimique> search(String libelle){
        return getSession().createCriteria(ClasseChimique.class)
                .add(Restrictions.ilike("libelle", libelle + "%"))
                .list();
    }
    */
}

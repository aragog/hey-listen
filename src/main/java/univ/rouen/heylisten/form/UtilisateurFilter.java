package univ.rouen.heylisten.form;

import univ.rouen.heylisten.util.UserType;

import java.io.Serializable;
import java.util.List;

public class UtilisateurFilter implements Serializable {
    private List<UserType> selectedTypes;

    public UtilisateurFilter() {
    }

    public UtilisateurFilter(List<UserType> selectedTypes) {
        this.selectedTypes = selectedTypes;
    }

    public List<UserType> getSelectedTypes() {
        return selectedTypes;
    }

    public void setSelectedTypes(List<UserType> selectedTypes) {
        this.selectedTypes = selectedTypes;
    }
}

package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.RegionDAO;
import univ.rouen.heylisten.ejb.Region;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by IuliaCristina on 12/18/2014.
 */

@Service
@Transactional
public class RegionService {
    @Autowired
    private RegionDAO dao;

    public Region getById(Long id) { return dao.getById(id); }

    public List<Region> getAll() { return dao.getAll(); }

    public Long create(Region resource) { return (Long) dao.create(resource); }

    public void update(Region resource) { dao.update(resource); }

    public void delete(Long id) { dao.delete(id); }
}
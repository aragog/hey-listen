package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.DispositifMedicalDAO;
import univ.rouen.heylisten.ejb.DispositifMedical;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class DispositifMedicalService {
    @Autowired
    private DispositifMedicalDAO dao;

    public DispositifMedical getById(Long id) {
        return dao.getById(id);
    }

    public List<DispositifMedical> getAll() {
        return dao.getAll();
    }

    public List<DispositifMedical> getAllOrderedByLibelle() {
        return dao.getAllOrderedByLibelle();
    }

    //public List<DispositifMedical> search(String libelle){ return dao.search(libelle); }

    public void index() throws Exception{ dao.index(); }

    public List<DispositifMedical> search(String searchText) throws Exception{ return  dao.search(searchText); }

    public Long create(DispositifMedical resource) {
        return (Long) dao.create(resource);
    }

    public void update(DispositifMedical resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

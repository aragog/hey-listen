package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.NotificationProduitDAO;
import univ.rouen.heylisten.ejb.NotificationProduit;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by IuliaCristina on 12/18/2014.
 */

@Service
@Transactional
public class NotificationProduitService {
    @Autowired
    private NotificationProduitDAO dao;

    public NotificationProduit getById(Long id) { return dao.getById(id); }

    public List<NotificationProduit> getAll() { return dao.getAll(); }

    public Long create(NotificationProduit resource) { return (Long) dao.create(resource); }

    public void update(NotificationProduit resource) { dao.update(resource); }

    public void delete(Long id) { dao.delete(id); }
}

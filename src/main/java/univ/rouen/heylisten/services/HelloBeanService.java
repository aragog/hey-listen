package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.HelloBeanDAO;
import univ.rouen.heylisten.ejb.HelloBean;

import java.util.List;

@Service
public class HelloBeanService {
	@Autowired
	private HelloBeanDAO dao;
	
	public List<HelloBean> getAll() {
		return dao.getAll();
	}
	
	public void create(HelloBean bean) {
		dao.create(bean);
	}
}

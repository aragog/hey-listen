package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.NotificationDispositifDAO;
import univ.rouen.heylisten.ejb.NotificationDispositif;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by IuliaCristina on 12/18/2014.
 */

@Service
@Transactional
public class NotificationDispositifService {
    @Autowired
    private NotificationDispositifDAO dao;

    public NotificationDispositif getById(Long id) { return dao.getById(id); }

    public List<NotificationDispositif> getAll() { return dao.getAll(); }

    public Long create(NotificationDispositif resource) { return (Long) dao.create(resource); }

    public void update(NotificationDispositif resource) { dao.update(resource); }

    public void delete(Long id) { dao.delete(id); }
}

package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.MedicamentDAO;
import univ.rouen.heylisten.ejb.Medicament;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class MedicamentService {
    @Autowired
    private MedicamentDAO dao;

    public Medicament getById(Long id) {
        return dao.getById(id);
    }

    public Medicament getByCodeCis(String codeCis) {
        return dao.getByCodeCis(codeCis);
    }

    public List<Medicament> getAll() {
        return dao.getAll();
    }

    public List<Medicament> getAllOrderedByLibelle() {
        return dao.getAllOrderedByLibelle();
    }

    public boolean hasAnyParents(Long id) {
        return dao.hasAnyParents(id);
    }

   // public List<Medicament> search(String libelle){ return dao.search(libelle); }

    public void index() throws Exception{ dao.index(); }

    public List<Medicament> search(String searchText) throws Exception{ return  dao.search(searchText); }

    public Long create(Medicament resource) {
        return (Long) dao.create(resource);
    }

    public void update(Medicament resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.LaboratoireCosmetiqueDAO;
import univ.rouen.heylisten.ejb.LaboratoireCosmetique;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class LaboratoireCosmetiqueService {
    @Autowired
    private LaboratoireCosmetiqueDAO dao;

    public LaboratoireCosmetique getById(Long id) {
        return dao.getById(id);
    }

    public List<LaboratoireCosmetique> getAll() {
        return dao.getAll();
    }

    public List<LaboratoireCosmetique> getAllOrderedByLibelle() {
        return dao.getAllOrderedByLibelle();
    }

    public boolean hasAnyParents(Long id) {
        return dao.hasAnyParents(id);
    }

    //public List<LaboratoireCosmetique> search(String libelle){ return dao.search(libelle); }

    public void index() throws Exception{ dao.index(); }

    public List<LaboratoireCosmetique> search(String searchText) throws Exception{ return  dao.search(searchText); }

    public Long create(LaboratoireCosmetique resource) {
        return (Long) dao.create(resource);
    }

    public void update(LaboratoireCosmetique resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

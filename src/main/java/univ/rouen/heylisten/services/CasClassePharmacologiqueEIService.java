package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.CasClassePharmacologiqueEIDAO;
import univ.rouen.heylisten.ejb.CasClassePharmacologiqueEI;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CasClassePharmacologiqueEIService {
    @Autowired
    private CasClassePharmacologiqueEIDAO dao;

    public CasClassePharmacologiqueEI getById(Long id) {
        return dao.getById(id);
    }

    public List<CasClassePharmacologiqueEI> getAll() {
        return dao.getAll();
    }

    public Long create(CasClassePharmacologiqueEI resource) {
        return (Long) dao.create(resource);
    }

    public void update(CasClassePharmacologiqueEI resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

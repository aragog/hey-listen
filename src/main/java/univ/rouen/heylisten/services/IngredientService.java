package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.IngredientDAO;
import univ.rouen.heylisten.ejb.Ingredient;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class IngredientService {
    @Autowired
    private IngredientDAO dao;

    public Ingredient getById(Long id) {
        return dao.getById(id);
    }

    public List<Ingredient> getAll() {
        return dao.getAll();
    }

    public List<Ingredient> getAllOrderedByLibelle() {
        return dao.getAllOrderedByLibelle();
    }

    public boolean hasAnyParents(Long id) { return dao.hasAnyParents(id); }

   // public List<Ingredient> search(String libelle){ return dao.search(libelle); }

    public void index() throws Exception{ dao.index(); }

    public List<Ingredient> search(String searchText) throws Exception{ return  dao.search(searchText); }

    public Long create(Ingredient resource) {
        return (Long) dao.create(resource);
    }

    public void update(Ingredient resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

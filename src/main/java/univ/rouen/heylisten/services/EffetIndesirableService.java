package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.EffetIndesirableDAO;
import univ.rouen.heylisten.ejb.EffetIndesirable;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class EffetIndesirableService {
    @Autowired
    private EffetIndesirableDAO dao;

    public EffetIndesirable getById(Long id) {
        return dao.getById(id);
    }

    public List<EffetIndesirable> getAll() {
        return dao.getAll();
    }

    public Long create(EffetIndesirable resource) {
        return (Long) dao.create(resource);
    }

    public void update(EffetIndesirable resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.CasClasseChimiqueEIDAO;
import univ.rouen.heylisten.ejb.CasClasseChimiqueEI;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CasClasseChimiqueEIService {
    @Autowired
    private CasClasseChimiqueEIDAO dao;

    public CasClasseChimiqueEI getById(Long id) {
        return dao.getById(id);
    }

    public List<CasClasseChimiqueEI> getAll() {
        return dao.getAll();
    }

    public Long create(CasClasseChimiqueEI resource) {
        return (Long) dao.create(resource);
    }

    public void update(CasClasseChimiqueEI resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

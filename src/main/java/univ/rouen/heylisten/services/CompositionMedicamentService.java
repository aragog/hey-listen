package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.CompositionMedicamentDAO;
import univ.rouen.heylisten.ejb.CompositionMedicament;

import java.util.List;

@Service
public class CompositionMedicamentService {
    @Autowired
    private CompositionMedicamentDAO dao;

    public CompositionMedicament getById(Long id) {
        return dao.getById(id);
    }

    public List<CompositionMedicament> getAll() {
        return dao.getAll();
    }

    public Long create(CompositionMedicament resource) {
        return (Long) dao.create(resource);
    }

    public void update(CompositionMedicament resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

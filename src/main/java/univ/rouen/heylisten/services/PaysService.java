package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.PaysDAO;
import univ.rouen.heylisten.ejb.Pays;

import java.util.List;

/**
 * Created by IuliaCristina on 12/18/2014.
 */

@Service
public class PaysService {
    @Autowired
    private PaysDAO dao;

    public Pays getById(Long id) { return dao.getById(id); }

    public List<Pays> getAll() { return dao.getAll(); }

    public Long create(Pays resource) { return (Long) dao.create(resource); }

    public void update(Pays resource) { dao.update(resource); }

    public void delete(Long id) { dao.delete(id); }
}
package univ.rouen.heylisten.services;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.UtilisateurDAO;
import univ.rouen.heylisten.ejb.Utilisateur;
import univ.rouen.heylisten.util.UserType;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UtilisateurService {
    @Autowired
    private UtilisateurDAO dao;

    public Utilisateur getById(Long id) {
        Utilisateur u = dao.getById(id);
        Hibernate.initialize(u.getNotificationDispositifs());
        Hibernate.initialize(u.getNotificationMedicaments());
        Hibernate.initialize(u.getNotificationProduits());
        return u;
    }

    public List<Utilisateur> getAll() {
        return dao.getAll();
    }

    public Utilisateur getByUsername(String username) {
        return dao.getByUsername(username);
    }

    public List<Utilisateur> getAllOrderedByUsername() {
        return dao.getAllOrderedByUsername();
    }

    public List<Utilisateur> getAllByRegionAndUserType(List<UserType> types, UserType userType, Long regionId) {
        if (org.assertj.core.util.Collections.isNullOrEmpty(types)) {
            return new ArrayList<>();
        }
        return dao.getAllByRegionAndUserTypes(types, userType, regionId);
    }

    public List<Utilisateur> getAllByRegion(Long regionId, UserType userType) {
        return dao.getAllByRegionAndUserType(regionId, userType);
    }

   // public List<Utilisateur> search(String username){ return dao.search(username); }

    public void index() throws Exception{ dao.index(); }

    public List<Utilisateur> search(String searchText) throws Exception{ return  dao.search(searchText); }

    public Long create(Utilisateur resource) {
        return (Long) dao.create(resource);
    }

    public void update(Utilisateur resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

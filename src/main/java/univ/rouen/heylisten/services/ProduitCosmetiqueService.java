package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.ProduitCosmetiqueDAO;
import univ.rouen.heylisten.ejb.ProduitCosmetique;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ProduitCosmetiqueService {
    @Autowired
    private ProduitCosmetiqueDAO dao;

    public ProduitCosmetique getById(Long id) {
        return dao.getById(id);
    }

    public ProduitCosmetique getByIdFull(Long id) {
        return dao.getByIdFull(id);
    }

    public List<ProduitCosmetique> getAll() {
        return dao.getAll();
    }

    public List<ProduitCosmetique> getAllOrderedByLibelle() {
        return dao.getAllOrderedByLibelle();
    }

    //public List<ProduitCosmetique> search(String libelle){ return dao.search(libelle); }

    public void index() throws Exception{ dao.index(); }

    public List<ProduitCosmetique> search(String searchText) throws Exception{ return  dao.search(searchText); }

    public Long create(ProduitCosmetique resource) {
        return (Long) dao.create(resource);
    }

    public void update(ProduitCosmetique resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

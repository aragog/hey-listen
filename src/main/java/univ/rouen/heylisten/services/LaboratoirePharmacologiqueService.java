package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.LaboratoirePharmacologiqueDAO;
import univ.rouen.heylisten.ejb.LaboratoirePharmacologique;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class LaboratoirePharmacologiqueService {
    @Autowired
    private LaboratoirePharmacologiqueDAO dao;

    public LaboratoirePharmacologique getById(Long id) {
        return dao.getById(id);
    }

    public List<LaboratoirePharmacologique> getAll() {
        return dao.getAll();
    }

    public List<LaboratoirePharmacologique> getAllOrderedByLibelle() {
        return dao.getAllOrderedByLibelle();
    }

    public boolean hasAnyParents(Long id) {
        return dao.hasAnyParents(id);
    }

    //public List<LaboratoirePharmacologique> search(String libelle){ return dao.search(libelle); }

    public void index() throws Exception{ dao.index(); }

    public List<LaboratoirePharmacologique> search(String searchText) throws Exception{ return  dao.search(searchText); }

    public Long create(LaboratoirePharmacologique resource) {
        return (Long) dao.create(resource);
    }

    public void update(LaboratoirePharmacologique resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

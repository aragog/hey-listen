package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.RoleDAO;
import univ.rouen.heylisten.ejb.Role;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class RoleService {
    @Autowired
    private RoleDAO dao;

    public Role getById(Long id) { return dao.getById(id); }

    public Role getRoleByName(String name) {
        return dao.getRoleByName(name);
    }

    public List<Role> getAll() { return dao.getAll(); }

    public Long create(Role resource) { return (Long) dao.create(resource); }

    public void update(Role resource) { dao.update(resource); }

    public void delete(Long id) { dao.delete(id); }
}

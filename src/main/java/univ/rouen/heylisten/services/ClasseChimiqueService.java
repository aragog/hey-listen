package univ.rouen.heylisten.services;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.ClasseChimiqueDAO;
import univ.rouen.heylisten.ejb.ClasseChimique;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ClasseChimiqueService {
    @Autowired
    private ClasseChimiqueDAO dao;

    public ClasseChimique getById(Long id) {
        return dao.getById(id);
    }

    public ClasseChimique getByIdClasseChimique(String idClasseChimique) {
        return dao.getByIdClasseChimique(idClasseChimique);
    }

    public ClasseChimique getByLibelle(String libelle) {
        return dao.getByLibelle(libelle);
    }

    public List<ClasseChimique> getAll() {
        return dao.getAll();
    }

    public List<ClasseChimique> getAllOrderedById() { return dao.getAllOrderedById(); }

    public boolean hasAnyParents(Long id) {
        return dao.hasAnyParents(id);
    }

    //public List<ClasseChimique> search(String libelle){ return dao.search(libelle); }

    public void index() throws Exception{ dao.index(); }

    public List<ClasseChimique> search(String searchText) throws Exception{ return  dao.search(searchText); }

    public Long create(ClasseChimique resource) { return (Long) dao.create(resource); }

    public void update(ClasseChimique resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }


}

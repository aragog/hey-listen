package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.SubstanceActiveDAO;
import univ.rouen.heylisten.ejb.SubstanceActive;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SubstanceActiveService {
    @Autowired
    private SubstanceActiveDAO dao;

    public SubstanceActive getById(Long id) {
        return dao.getById(id);
    }

    public List<SubstanceActive> getAll() {
        return dao.getAll();
    }

    public List<SubstanceActive> getAllOrderedByLibelle() {
        return dao.getAllOrderedByLibelle();
    }

    public boolean hasAnyParents(Long id) {
        return dao.hasAnyParents(id);
    }

    //public List<SubstanceActive> search(String libelle){ return dao.search(libelle); }

    public void index() throws Exception{ dao.index(); }

    public List<SubstanceActive> search(String searchText) throws Exception{ return  dao.search(searchText); }

    public Long create(SubstanceActive resource) {
        return (Long) dao.create(resource);
    }

    public void update(SubstanceActive resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.ClassePharmacologiqueDAO;
import univ.rouen.heylisten.ejb.ClassePharmacologique;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ClassePharmacologiqueService {
    @Autowired
    private ClassePharmacologiqueDAO dao;

    public ClassePharmacologique getById(Long id) {
        return dao.getById(id);
    }

    public ClassePharmacologique getByIdClassePharmacologique(String idClassePharmacologique) {
        return dao.getByIdClassePharmacologique(idClassePharmacologique);
    }

    public ClassePharmacologique getByLibelle(String libelle) {
        return dao.getByLibelle(libelle);
    }

    public List<ClassePharmacologique> getAll() {
        return dao.getAll();
    }

    public List<ClassePharmacologique> getAllOrderedById() { return dao.getAllOrderedById(); }

    public boolean hasAnyParents(Long id) { return dao.hasAnyParents(id); }

    //public List<ClassePharmacologique> search(String libelle){ return dao.search(libelle); }

    public void index() throws Exception{ dao.index(); }

    public List<ClassePharmacologique> search(String searchText) throws Exception{ return  dao.search(searchText); }

    public Long create(ClassePharmacologique resource) { return (Long) dao.create(resource); }

    public void update(ClassePharmacologique resource) {
        dao.update(resource);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}

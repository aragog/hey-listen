package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.CasSubstanceEIDAO;
import univ.rouen.heylisten.ejb.CasSubstanceEI;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by IuliaCristina on 12/18/2014.
 */

@Service
@Transactional
public class CasSubstanceEIService {
    @Autowired
    private CasSubstanceEIDAO dao;

    public CasSubstanceEI getById(Long id) { return dao.getById(id); }

    public List<CasSubstanceEI> getAll() { return dao.getAll(); }

    public Long create(CasSubstanceEI resource) { return (Long) dao.create(resource); }

    public void update(CasSubstanceEI resource) { dao.update(resource); }

    public void delete(Long id) { dao.delete(id); }
}

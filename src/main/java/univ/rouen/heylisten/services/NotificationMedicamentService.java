package univ.rouen.heylisten.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.rouen.heylisten.dao.NotificationMedicamentDAO;
import univ.rouen.heylisten.ejb.NotificationMedicament;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by IuliaCristina on 12/18/2014.
 */

@Service
@Transactional
public class NotificationMedicamentService {
    @Autowired
    private NotificationMedicamentDAO dao;

    public NotificationMedicament getById(Long id) { return dao.getById(id); }

    public List<NotificationMedicament> getAll() { return dao.getAll(); }

    public Long create(NotificationMedicament resource) { return (Long) dao.create(resource); }

    public void update(NotificationMedicament resource) { dao.update(resource); }

    public void delete(Long id) { dao.delete(id); }
}

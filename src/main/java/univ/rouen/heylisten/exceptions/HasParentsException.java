package univ.rouen.heylisten.exceptions;

public class HasParentsException extends RuntimeException {
    public HasParentsException(String message) {
        super(message);
    }
}

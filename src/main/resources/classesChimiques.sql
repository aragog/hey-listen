INSERT INTO ClasseChimique(idClasseChimique, libelle) VALUES ('A','Cationiques');
INSERT INTO ClasseChimique(idClasseChimique, libelle, classeChimiquePere_id) VALUES ('A1','Calcium derive', 1);
INSERT INTO ClasseChimique(idClasseChimique, libelle, classeChimiquePere_id) VALUES ('A2','Magnesium derive', 1);
INSERT INTO ClasseChimique(idClasseChimique, libelle, classeChimiquePere_id) VALUES ('A3','Manganese derive', 1);
INSERT INTO ClasseChimique(idClasseChimique, libelle, classeChimiquePere_id) VALUES ('A4','Potassium derive', 1);
INSERT INTO ClasseChimique(idClasseChimique, libelle, classeChimiquePere_id) VALUES ('A5','Sodium derive', 1);



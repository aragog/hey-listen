INSERT INTO Medicament (code_cis, libelle) VALUES (68586203, '17 B ESTRADIOL BESINS-ISCOVESCO 0,06 POUR CENT, gel pour application cutanee en tube');
INSERT INTO Medicament (code_cis, libelle) VALUES (63068541, '5-MONONITRATE D''ISOSORBIDE ETHYPHARM LP 20 mg, gelule e liberation prolongee');
INSERT INTO Medicament (code_cis, libelle) VALUES (68166740, '5-MONONITRATE D''ISOSORBIDE ETHYPHARM LP 40 mg, gelule e liberation prolongee');
INSERT INTO Medicament (code_cis, libelle) VALUES (69681953, '5-MONONITRATE D''ISOSORBIDE ETHYPHARM LP 60 mg, gelule e liberation prolongee');
INSERT INTO Medicament (code_cis, libelle) VALUES (61266250, 'A 313 200 000 UI POUR CENT, pommade');
INSERT INTO Medicament (code_cis, libelle) VALUES (62869109, 'A 313 50 000 U.I., capsule molle');
INSERT INTO Medicament (code_cis, libelle) VALUES (61855042, 'A B I, emulsion pour application locale');
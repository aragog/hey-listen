INSERT INTO ClassePharmacologique(idClassePharmacologique, libelle) VALUES ('A','Classe specifique specialite');
INSERT INTO ClassePharmacologique(idClassePharmacologique, libelle, classePharmacologiquePere_id) VALUES ('AA','Antalgiques/Anagesiques', 1);
INSERT INTO ClassePharmacologique(idClassePharmacologique, libelle, classePharmacologiquePere_id) VALUES ('AB','Anesthesiques locaux', 1);
INSERT INTO ClassePharmacologique(idClassePharmacologique, libelle, classePharmacologiquePere_id) VALUES ('AB1','Anastesique local conduction', 3);
INSERT INTO ClassePharmacologique(idClassePharmacologique, libelle, classePharmacologiquePere_id) VALUES ('AB2','Anastesique local contact', 3);
INSERT INTO ClassePharmacologique(idClassePharmacologique, libelle, classePharmacologiquePere_id) VALUES ('AB3','Anastesique local infiltration', 3);



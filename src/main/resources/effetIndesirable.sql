INSERT INTO EffetIndesirable(libelle, premiereDeclaration, derniereDeclaration) VALUES ('Cholesterolemie', '2015-01-01', '2015-01-17');
INSERT INTO EffetIndesirable(libelle, premiereDeclaration, derniereDeclaration) VALUES ('Cholesterolemie_anormale', '2015-01-01', '2015-01-17');
INSERT INTO EffetIndesirable(libelle, premiereDeclaration, derniereDeclaration) VALUES ('Cholesterolemie_diminuee', '2015-01-01', '2015-01-15');
INSERT INTO EffetIndesirable(libelle, premiereDeclaration, derniereDeclaration) VALUES ('Cholesterolemie_augmentee', '2015-01-01', '2015-01-16');
INSERT INTO EffetIndesirable(libelle, premiereDeclaration, derniereDeclaration) VALUES ('Cholesterolemie_normale', '2015-01-01', '2015-01-14');

INSERT INTO CasSubstanceEI(effetIndesirableSubstance_id, substance_idSubstance) VALUES (1, 140);
INSERT INTO CasSubstanceEI(effetIndesirableSubstance_id, substance_idSubstance) VALUES (2, 140);
INSERT INTO CasSubstanceEI(effetIndesirableSubstance_id, substance_idSubstance) VALUES (3, 140);
INSERT INTO CasSubstanceEI(effetIndesirableSubstance_id, substance_idSubstance) VALUES (3, 428);
INSERT INTO CasSubstanceEI(effetIndesirableSubstance_id, substance_idSubstance) VALUES (3, 19809);
INSERT INTO CasSubstanceEI(effetIndesirableSubstance_id, substance_idSubstance) VALUES (4, 19809);
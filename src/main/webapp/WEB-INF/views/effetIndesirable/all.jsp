<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>Effets ind&eacute;sirables</title>
</head>
<body>
<sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')" var="connected">
    <c:import url="../menu.jsp" />
</sec:authorize>

<div class="container-fluid">
    <div class="row col-md-12">
        <p class="text-center text-primary">${fn:length(effetsIndesirables)} r&eacute;sultat(s)</p>
    </div>

    <table class="table row col-md-12">
        <thead>
        <tr>
            <th>Libell&eacute;</th>
            <th>Derni&egrave;re d&eacute;claration</th>
            <th>Action</th>
        </tr>
        </thead>
        <c:forEach items="${effetsIndesirables}" var="effetIndesirable">
            <tbody>
            <tr>
                <td>
                    <c:choose>
                        <c:when test="${connected}">
                            <a href="<c:url value="/effetindesirable/detail?id=${effetIndesirable.idEffetIndesirable}" />">${effetIndesirable.libelle}</a>
                        </c:when>
                        <c:otherwise>${effetIndesirable.libelle}</c:otherwise>
                    </c:choose>
                </td>
                <td>${effetIndesirable.derniereDeclaration}</td>
                <td>
                    <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')" var="connected">
                        <a href="<c:url value="/effetindesirable/detail?id=${effetIndesirable.idEffetIndesirable}" />" class="btn btn-sm btn-info">Consulter</a>
                        <a href="<c:url value="/effetindesirable/delete?id=${effetIndesirable.idEffetIndesirable}" />" class="btn btn-sm btn-danger">Supprimer</a>
                    </sec:authorize>
                </td>
            </tr>
            </tbody>
        </c:forEach>
    </table>

    <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')">
        <div class="row col-md-12">
            <p><a href="<c:url value="/effetindesirable/creer"/>" class="btn btn-default">Ajouter un effet ind&eacute;sirable</a></p>
        </div>
    </sec:authorize>
</div>
</body>
</html>
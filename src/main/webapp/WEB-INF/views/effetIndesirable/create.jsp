<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>Nouvel effet ind&eacute;sirable</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
    <div class="row col-md-4">
        <form:form commandName="newEI" action="ajouterEffetIndesirable">
            <div class="form-group">
                <form:label path="libelle">Libell&eacute; de l'effet ind&eacute;sirable</form:label>
                <form:input path="libelle" cssClass="form-control" required="required" />
                <form:errors path="libelle" cssClass="has-error" />
            </div>
            <a href="<c:url value="/effetindesirable/all" />" class="btn btn-warning">Retour</a>
            <input type="submit" class="btn btn-primary" value="Ajouter" />
        </form:form>
    </div>
</div>
</body>
</html>

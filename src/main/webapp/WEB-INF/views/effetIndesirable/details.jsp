<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>D&eacute;tails de l'effet ind&eacute;sirable</title>
</head>
<body>
<c:import url="../menu.jsp" />
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Nom : <c:out value="${effetIndesirable.libelle}" /></h3>
        </div>
    </div>
    <a href="<c:url value="/effetindesirable/all" />" class="btn btn-warning">Retour</a>
    <a class="btn btn-default" href="<c:url value="/effetindesirable/update/${effetIndesirable.idEffetIndesirable}" />">Modifier</a>
</div>
</body>
</html>

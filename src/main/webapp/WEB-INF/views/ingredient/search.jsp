<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
  <title>Ingr&eacute;dients</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
  <div <c:if test="${empty error}">style="display: none;" </c:if>>
    <div class="alert alert-danger alert-dismissible fade <c:if test="${!empty error}">in</c:if>" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <h4>Suppression impossible</h4>
      <p>${error.message}</p>
    </div>
  </div>

  <div class="row col-md-12">
    <p class="text-center text-primary">${fn:length(ingredientsRecherche)} r&eacute;sultat(s)</p>
  </div>

<form:form commandName="searchText" action="search">
  <table class="table row col-md-12">
    <thead>
    <tr>
      <th>Libell&eacute;</th>
      <th>Action</th>
    </tr>
    </thead>

  <input name="searchText" type="text">
  <input type="submit" class="btn btn-sm btn-info" value="Rechercher" />
  <a href="<c:url value="/ingredient/all" />" class="btn btn-sm btn-danger">Retour</a>

  <c:if test="${! empty ingredientsRecherche}">
    <c:forEach items="${ingredientsRecherche}" var="ingredient">
      <tbody>
      <tr>
        <td><a href="<c:url value="/ingredient/detail?id=${ingredient.idIngredient}" />">${ingredient.libelle}</a></td>
        <td>
          <a href="<c:url value="/ingredient/detail?id=${ingredient.idIngredient}" />" class="btn btn-sm btn-info">Consulter</a>
          <a href="<c:url value="/ingredient/delete?id=${ingredient.idIngredient}" />" class="btn btn-sm btn-danger" onclick="confirmMessage()">Supprimer</a>
        </td>
      </tr>
      </tbody>
    </c:forEach>
  </c:if>

  <c:if test="${empty ingredientsRecherche}">
    <p> Aucun résultat trouvé.</p>
  </c:if>
  </table>
</form:form>

</div>
</body>
<script>
  function confirmMessage() {
    if (confirm("Etes vous sur de vouloir supprimer ?")) {
      window.location.href = "/ingredient/delete?id=${ingredient.idIngredient}";
    }
  }
</script>
</html>

<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>D&eacute;tails de la substance</title>
</head>
<body>
<c:import url="../menu.jsp" />
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Substance active</h3>
        </div>
        <div class="panel-body">
            <p>Nom : <c:out value="${substanceActive.libelle}" /></p>
        </div>
    </div>
    <a href="<c:url value="/substanceActive/all" />" class="btn btn-warning">Retour</a>
    <a class="btn btn-default" href="<c:url value="/substanceActive/update/${substanceActive.idSubstance}" />">Modifier</a>
</div>
</body>
</html>

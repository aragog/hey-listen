<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>Mise &agrave; jour</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
    <div class="row col-md-4">
        <form:form commandName="user" action="miseajour">
            <div class="form-group">
                <form:label path="username">Nom d'utilisateur</form:label>
                <form:input path="username" cssClass="form-control" required="required" />
                <form:errors path="username" cssClass="has-error" />
            </div>

            <div class="form-group">
                <form:label path="passwd">Mot de passe</form:label>
                <form:password path="passwd" cssClass="form-control" required="required" />
                <form:errors path="passwd" cssClass="has-error" />
            </div>

            <div class="form-group">
                <form:label path="region">R&eacute;gion</form:label>
                <form:select path="region" cssClass="form-control" items="${regions}" itemValue="idRegion" multiple="false" />
                <form:errors path="region" cssClass="has-error" />
            </div>

            <div class="form-group">
                <form:label path="type">Type d'utilisateur</form:label>
                <form:select path="type" cssClass="form-control" items="${userTypes}" multiple="false" />
                <form:errors path="type" cssClass="has-error" />
            </div>

            <a href="<c:url value="/utilisateur/all" />" class="btn btn-warning">Retour</a>
            <input type="submit" class="btn btn-primary" value="Modifier" />
        </form:form>
    </div>
</div>
</body>
</html>

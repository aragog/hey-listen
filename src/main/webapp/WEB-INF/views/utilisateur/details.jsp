<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>D&eacute;tails du client</title>
</head>
<body>
    <c:import url="../menu.jsp" />
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><c:out value="${user.username}" /></h3>
            </div>
            <div class="panel-body">
                <p>Type d'utilisateur : <c:out value="${user.type}" /></p>
                <p>Pays : <c:out value="${user.region.pays.libelle}" /></p>
                <p>R&eacute;gion : <c:out value="${user.region.libelle}" /></p>
                <p>Notifications m&eacute;dicaments : <c:out value="${fn:length(user.notificationMedicaments)}" /></p>
                <p>Notifications produits cosm&eacute;tiques : <c:out value="${fn:length(user.notificationProduits)}" /></p>
                <p>Notifications dispositifs m&eacute;dicaux : <c:out value="${fn:length(user.notificationDispositifs)}" /></p>
            </div>
        </div>
        <a href="<c:url value="/utilisateur/all" />" class="btn btn-warning">Retour</a>

        <sec:authorize access="hasAnyRole('ROLE_ADMIN')">
            <a class="btn btn-default" href="<c:url value="/utilisateur/update/${user.idUtilisateur}" />">Modifier profil</a>
        </sec:authorize>
    </div>
</body>
</html>

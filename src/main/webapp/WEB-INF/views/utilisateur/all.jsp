<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>Utilisateurs</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
    <div class="row col-md-12">
        <form:form cssClass="form-inline" commandName="filter" action="filter">
            <c:forEach items="${userTypes}" var="userType">
                <form:label path="selectedTypes">
                    <div class="checkbox">
                        <form:checkbox path="selectedTypes" value="${userType}" />
                            ${userType}
                    </div>
                </form:label>
            </c:forEach>
            <div>
                <input class="btn btn-primary" type="submit" value="Filtrer" />
                <a class="btn btn-warning" href="<c:url value="/utilisateur/reinit" />">Tous</a>
                <a class="btn btn-warning" href="<c:url value="/utilisateur/selectNone" />">R&eacute;initialiser</a>
            </div>
        </form:form>
    </div>

    <div class="row col-md-12">
        <p class="text-center text-primary">${fn:length(users)} r&eacute;sultat(s)</p>
    </div>

    <table class="table row col-md-12">
        <thead>
        <tr>
            <th>Nom d'utilisateur</th>
            <th>Type</th>
            <th>R&eacute;gion</th>
            <th>Action</th>
        </tr>
        </thead>

        <form action="search">
            <input name="searchText" type="text">
            <a href="<c:url value="/utilisateur/search?searchText=${searchText}" />" class="btn btn-sm btn-info">Rechercher</a>
        </form>

        <c:forEach items="${users}" var="user">
            <tbody>
            <tr>
                <td><a href="<c:url value="/utilisateur/detail?id=${user.idUtilisateur}" />">${user.username}</a></td>
                <td>${user.type}</td>
                <td>${user.region.libelle}</td>
                <td>
                    <a href="<c:url value="/utilisateur/detail?id=${user.idUtilisateur}" />" class="btn btn-sm btn-info">Consulter</a>
                    <sec:authorize access="hasAnyRole('ROLE_ADMIN')">
                        <a href="<c:url value="/utilisateur/delete?id=${user.idUtilisateur}" />" class="btn btn-sm btn-danger" onclick="confirmMessage()">Supprimer</a>
                    </sec:authorize>
                </td>
            </tr>
            </tbody>
        </c:forEach>
    </table>

    <div class="row col-md-12">
        <p><a href="<c:url value="/utilisateur/creer"/>" class="btn btn-default">Ajouter un nouvel utilisateur</a></p>
    </div>
</div>
</body>
<script>
    function confirmMessage() {
        if (confirm("Etes vous sur de vouloir supprimer ?")) {
            window.location.href = "/utilisateur/delete?id=${user.idUtilisateur}";
        }
    }
</script>
</html>

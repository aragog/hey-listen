<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
  <title>Dispositifs m&eacute;dicaux</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
  <div class="row col-md-12">
    <p class="text-center text-primary">${fn:length(dispositifsMedicauxRecherche)} r&eacute;sultat(s)</p>
  </div>

  <form:form commandName="searchText" action="search">
  <table class="table row col-md-12">
    <thead>
    <tr>
      <th>Libell&eacute;</th>
      <th>Laboratoire</th>
      <th>Action</th>
    </tr>
    </thead>

    <input name="searchText" type="text">
    <input type="submit" class="btn btn-sm btn-info" value="Rechercher" />
    <a href="<c:url value="/dispositifMedical/all" />" class="btn btn-sm btn-danger">Retour</a>

    <c:if test="${! empty dispositifsMedicauxRecherche}">
      <c:forEach items="${dispositifsMedicauxRecherche}" var="dispositifMedical">
      <tbody>
      <tr>
        <td><a href="<c:url value="/dispositifMedical/detail?id=${dispositifMedical.idDispositif}" />">${dispositifMedical.libelle}</a></td>
        <td>${dispositifMedical.labPharm.libelle}</td>
        <td>
          <a href="<c:url value="/dispositifMedical/detail?id=${dispositifMedical.idDispositif}" />" class="btn btn-sm btn-info">Consulter</a>
          <a href="<c:url value="/dispositifMedical/delete?id=${dispositifMedical.idDispositif}" />" class="btn btn-sm btn-danger" onclick="confirmMessage()">Supprimer</a>
        </td>
      </tr>
      </tbody>
      </c:forEach>
    </c:if>

    <c:if test="${empty dispositifsMedicauxRecherche}">
    <p> Aucun résultat trouvé.</p>
    </c:if>

  </table>
  </form:form>
</div>
</body>
<script>
  function confirmMessage() {
    if (confirm("Etes vous sur de vouloir supprimer ?")) {
      window.location.href = "/dispositifMedical/delete?id=${dispositifMedical.idDispositif}";
    }
  }
</script>
</html>

<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>D&eacute;tails du laboratoire</title>
</head>
<body>
<c:import url="../menu.jsp" />
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Laboratoire pharmacologique</h3>
        </div>
        <div class="panel-body">
            <p>Nom : <c:out value="${laboratoirePharmacologique.libelle}" /></p>
        </div>
    </div>
    <a href="<c:url value="/laboratoirePharmacologique/all" />" class="btn btn-warning">Retour</a>
    <a class="btn btn-default" href="<c:url value="/laboratoirePharmacologique/update/${laboratoirePharmacologique.idLabPharm}" />">Modifier</a>
</div>
</body>
</html>

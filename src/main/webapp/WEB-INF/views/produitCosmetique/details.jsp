<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>D&eacute;tails du produit</title>
</head>
<body>
<c:import url="../menu.jsp" />
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><c:out value="${produitCosmetique.libelle}" /></h3>
        </div>
        <div class="panel-body">
            <p>Nom : <c:out value="${produitCosmetique.labCosm.libelle}" /></p>
            <p>Ingr&eacute;dients :</p>
            <ul>
                <c:forEach items="${produitCosmetique.ingredients}" var="ingredient">
                    <li>${ingredient.libelle}</li>
                </c:forEach>
            </ul>
        </div>
    </div>
    <a href="<c:url value="/produitCosmetique/all" />" class="btn btn-warning">Retour</a>
    <a class="btn btn-default" href="<c:url value="/produitCosmetique/update/${produitCosmetique.idProduit}" />">Modifier</a>
</div>
</body>
</html>

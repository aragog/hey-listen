<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>Produits cosm&eacute;tiques</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
    <div class="row col-md-12">
        <p class="text-center text-primary">${fn:length(produitsCosmetiques)} r&eacute;sultat(s)</p>
    </div>

    <table class="table row col-md-12">
        <thead>
        <tr>
            <th>Libell&eacute;</th>
            <th>Laboratoire</th>
            <th>Action</th>
        </tr>
        </thead>

        <form action="search">
            <input name="searchText" type="text">
            <a href="<c:url value="/produitCosmetique/search?searchText=${searchText}" />" class="btn btn-sm btn-info">Rechercher</a>
        </form>

        <c:forEach items="${produitsCosmetiques}" var="produitCosmetique">
            <tbody>
            <tr>
                <td><a href="<c:url value="/produitCosmetique/detail?id=${produitCosmetique.idProduit}" />">${produitCosmetique.libelle}</a></td>
                <td>${produitCosmetique.labCosm.libelle}</td>
                <td>
                    <a href="<c:url value="/produitCosmetique/detail?id=${produitCosmetique.idProduit}" />" class="btn btn-sm btn-info">Consulter</a>
                    <a href="<c:url value="/produitCosmetique/delete?id=${produitCosmetique.idProduit}" />" class="btn btn-sm btn-danger" onclick="confirmMessage()">Supprimer</a>
                </td>
            </tr>
            </tbody>
        </c:forEach>
    </table>

    <div class="row col-md-12">
        <p><a href="<c:url value="/produitCosmetique/creer"/>" class="btn btn-default">Ajouter un nouveau produit</a></p>
    </div>
</div>
</body>
<script>
    function confirmMessage() {
        if (confirm("Etes vous sur de vouloir supprimer ?")) {
            window.location.href = "/produitCosmetique/delete?id=${produitCosmetique.idProduit}";
        }
    }
</script>
</html>

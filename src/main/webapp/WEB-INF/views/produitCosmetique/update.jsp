<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>Mise &agrave; jour</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
    <div class="row col-md-4">
        <form:form commandName="produitCosmetique" action="miseajour">
            <div class="form-group">
                <form:label path="libelle">Libell&eacute;</form:label>
                <form:input path="libelle" cssClass="form-control" required="required" />
                <form:errors path="libelle" cssClass="has-error" />
            </div>

            <div class="form-group">
                <form:label path="labCosm">Laboratoire cosm&eacute;tique</form:label>
                <form:select path="labCosm" cssClass="form-control" items="${laboratoires}" itemValue="idLabCosm" multiple="false" />
                <form:errors path="labCosm" cssClass="has-error" />
            </div>

            <div class="form-group">
                <form:label path="ingredients">Ingr&eacute;dients</form:label>
                <form:select path="ingredients" cssClass="form-control" items="${ingredients}" itemValue="idIngredient" />
                <form:errors path="ingredients" cssClass="has-error" />
            </div>

            <a href="<c:url value="/produitCosmetique/all" />" class="btn btn-warning">Retour</a>
            <input type="submit" class="btn btn-primary" value="Modifier" />
        </form:form>
    </div>
</div>
</body>
</html>
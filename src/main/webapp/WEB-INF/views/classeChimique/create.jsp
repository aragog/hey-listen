<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
  <title>Nouvelle classe chimique</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
  <div class="row col-md-4">
    <form:form commandName="newclasseChimique" action="ajouter">
      <div class="form-group">
        <form:label path="idClasseChimique">ID classe chimique</form:label>
        <form:input path="idClasseChimique" cssClass="form-control" required="required" />
        <form:errors path="idClasseChimique" cssClass="has-error" />
      </div>

      <div class="form-group">
        <form:label path="libelle">Libell&eacute;</form:label>
        <form:input path="libelle" cssClass="form-control" required="required" />
        <form:errors path="libelle" cssClass="has-error" />
      </div>

      <div class="form-group">
        <form:label path="classeChimiquePere">Classes chimiques </form:label>
        <form:select path="classeChimiquePere" cssClass="form-control" items="${classesChimiques}" itemValue="id" multiple="false" />
        <form:errors path="classeChimiquePere" cssClass="has-error" />
      </div>

      <a href="<c:url value="/classeChimique/all" />" class="btn btn-warning">Retour</a>
      <input type="submit" class="btn btn-primary" value="Ajouter" />
    </form:form>
  </div>
</div>
</body>
</html>

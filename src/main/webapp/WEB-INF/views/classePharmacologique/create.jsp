<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
  <title>Nouvelle classe pharmacologique</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
  <div class="row col-md-4">
    <form:form commandName="newclassePharmacologique" action="ajouter">
      <div class="form-group">
        <form:label path="idClassePharmacologique">ID classe pharmacologique</form:label>
        <form:input path="idClassePharmacologique" cssClass="form-control" required="required" />
        <form:errors path="idClassePharmacologique" cssClass="has-error" />
      </div>

      <div class="form-group">
        <form:label path="libelle">Libell&eacute;</form:label>
        <form:input path="libelle" cssClass="form-control" required="required" />
        <form:errors path="libelle" cssClass="has-error" />
      </div>

      <div class="form-group">
        <form:label path="classePharmacologiquePere">Classes pharmacologiques </form:label>
        <form:select path="classePharmacologiquePere" cssClass="form-control" items="${classesPharmacologiques}" itemValue="id" multiple="false" />
        <form:errors path="classePharmacologiquePere" cssClass="has-error" />
      </div>

      <a href="<c:url value="/classePharmacologique/all" />" class="btn btn-warning">Retour</a>
      <input type="submit" class="btn btn-primary" value="Ajouter" />
    </form:form>
  </div>
</div>
</body>
</html>

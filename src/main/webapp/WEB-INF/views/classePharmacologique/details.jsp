<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
  <title>D&eacute;tails de la classe pharmacologique</title>
</head>
<body>
<c:import url="../menu.jsp" />
<div class="container-fluid">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title"><c:out value="${classePharmacologique.libelle}" /></h3>
    </div>
    <div class="panel-body">
      <p>ID : <c:out value="${classePharmacologique.idClassePharmacologique}" /></p>
      <c:if test="${classePharmacologique.classePharmacologiquePere.id != null}">
        <p>ID classe pharmacologique m&egrave;re : <c:out value="${classePharmacologique.classePharmacologiquePere.idClassePharmacologique}" /></p>
        <p>Libell&eacute; classe pharmacologique m&egrave;re : <c:out value="${classePharmacologique.classePharmacologiquePere.libelle}" /></p>
      </c:if>
    </div>
  </div>
  <a href="<c:url value="/classePharmacologique/all" />" class="btn btn-warning">Retour</a>
  <a class="btn btn-default" href="<c:url value="/classePharmacologique/update/${classePharmacologique.id}" />">Modifier</a>
</div>
</body>
</html>

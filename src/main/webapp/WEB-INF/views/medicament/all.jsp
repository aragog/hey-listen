<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>M&eacute;dicaments</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
    <div class="row col-md-12">
        <p class="text-center text-primary">${fn:length(medicaments)} r&eacute;sultat(s)</p>
    </div>

    <table class="table row col-md-12">
        <thead>
        <tr>
            <th>Code CIS</th>
            <th>Libell&eacute;</th>
            <th>Action</th>
        </tr>
        </thead>

        <form action="search">
            <input name="searchText" type="text">
            <a href="<c:url value="/medicament/search?searchText=${searchText}" />" class="btn btn-sm btn-info">Rechercher</a>
        </form>

        <c:forEach items="${medicaments}" var="medicament">
            <tbody>
            <tr>
                <td><a href="<c:url value="/medicament/detail?id=${medicament.idMedicament}" />">${medicament.code_cis}</a></td>
                <td>${medicament.libelle}</td>
                <td>
                    <a href="<c:url value="/medicament/detail?id=${medicament.idMedicament}" />" class="btn btn-sm btn-info">Consulter</a>
                    <a href="<c:url value="/medicament/delete?id=${medicament.idMedicament}" />" class="btn btn-sm btn-danger" onclick="confirmMessage()">Supprimer</a>
                </td>
            </tr>
            </tbody>
        </c:forEach>
    </table>

    <div class="row col-md-12">
        <p><a href="<c:url value="/medicament/creer"/>" class="btn btn-default">Ajouter un nouveau m&eacute;dicament</a></p>
    </div>
</div>
</body>
<script>
    function confirmMessage() {
        if (confirm("Etes vous sur de vouloir supprimer ?")) {
            window.location.href = "/medicament/delete?id=${medicament.id}";
        }
    }
</script>
</html>

<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
  <title>M&eacute;dicaments</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
  <div class="row col-md-12">
    <p class="text-center text-primary">${fn:length(medicamentsRecherche)} r&eacute;sultat(s)</p>
  </div>

<form:form commandName="searchText" action="search">
  <table class="table row col-md-12">
    <thead>
    <tr>
      <th>Code CIS</th>
      <th>Libell&eacute;</th>
      <th>Action</th>
    </tr>
    </thead>

  <input name="searchText" type="text">
  <input type="submit" class="btn btn-sm btn-info" value="Rechercher" />
  <a href="<c:url value="/medicament/all" />" class="btn btn-sm btn-danger">Retour</a>

  <c:if test="${! empty medicamentsRecherche}">
    <c:forEach items="${medicamentsRecherche}" var="medicament">
      <tbody>
      <tr>
        <td><a href="<c:url value="/medicament/detail?id=${medicament.id}" />">${medicament.code_cis}</a></td>
        <td>${medicament.libelle}</td>
        <td>
          <a href="<c:url value="/medicament/detail?id=${medicament.id}" />" class="btn btn-sm btn-info">Consulter</a>
          <a href="<c:url value="/medicament/delete?id=${medicament.id}" />" class="btn btn-sm btn-danger" onclick="confirmMessage()">Supprimer</a>
        </td>
      </tr>
      </tbody>
    </c:forEach>
  </c:if>

    <c:if test="${empty medicamentsRecherche}">
      <p> Aucun résultat trouvé.</p>
    </c:if>

  </table>
</form:form>

</div>
</body>
<script>
  function confirmMessage() {
    if (confirm("Etes vous sur de vouloir supprimer ?")) {
      window.location.href = "/medicament/delete?id=${medicament.id}";
    }
  }
</script>
</html>

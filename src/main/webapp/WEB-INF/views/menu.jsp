<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<ul class="nav nav-tabs">
    <li><a href="<c:url value="/index" />">Home</a></li>
    <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_REGIONAL', 'ROLE_NATIONAL')">
        <li><a href="<c:url value="/utilisateur/all" />">Utilisateurs</a></li>
    </sec:authorize>
    <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
            Notification d'effet ind&eacute;sirable <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="<c:url value="/notification/allSubstance" />">Cas connus pour les substances actives</a></li>
            <li><a href="<c:url value="/notification/allChimique" />">Cas connus pour les classes chimiques</a></li>
            <li><a href="<c:url value="/notification/allPharmacologique" />">Cas connus pour les classes pharmacologiques</a></li>
            <li><a href="<c:url value="/notification/allMedicament" />">Notification d'effet ind&eacute;sirable pour les medicaments</a></li>
            <li><a href="<c:url value="/notification/allDispositif" />">Notification d'effet ind&eacute;sirable pour les dispositifs medicaux</a></li>
            <li><a href="<c:url value="/notification/allProduit" />">Notification d'effet ind&eacute;sirable pour les produits cosmétiques</a></li>
        </ul>
    </li>
    <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
            Entit&eacute;s <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="<c:url value="/medicament/all" />">M&eacute;dicaments</a></li>
            <li><a href="<c:url value="/dispositifMedical/all" />">Dispositifs m&eacute;dicaux</a></li>
            <li><a href="<c:url value="/produitCosmetique/all" />">Produits cosm&eacute;tiques</a></li>
            <li><a href="<c:url value="/effetindesirable/all" />">Effets ind&eacute;sirables</a></li>
        </ul>
    </li>
    <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
            Laboratoires <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="<c:url value="/laboratoirePharmacologique/all" />">Laboratoires pharmacologiques</a></li>
            <li><a href="<c:url value="/laboratoireCosmetique/all" />">Laboratoires cosm&eacute;tiques</a></li>
        </ul>
    </li>
    <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
            Ingr&eacute;dients/substances <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="<c:url value="/ingredient/all" />">Ingr&eacute;dients</a></li>
            <li><a href="<c:url value="/substanceActive/all" />">Substances actives</a></li>
        </ul>
    </li>
    <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
            Classes <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="<c:url value="/classeChimique/all" />">Classes chimiques</a></li>
            <li><a href="<c:url value="/classePharmacologique/all" />">Classes pharmacologiques</a></li>
        </ul>
    </li>
    <li>
        <c:url value="/logout" var="logoutUrl" />
        <script>
            function formSubmit() {
                document.getElementById("logoutForm").submit();
            }
        </script>
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <a href="javascript:formSubmit()">D&eacute;connexion</a>
        </c:if>
    </li>
    <!-- csrt for log out-->
    <form action="${logoutUrl}" method="post" id="logoutForm">
        <input type="hidden"
               name="${_csrf.parameterName}"
               value="${_csrf.token}" />
    </form>
</ul>

<script src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script src="https://code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
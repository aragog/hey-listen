<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8"/>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>Page de connexion</title>
</head>
<body onload='document.loginForm.username.focus();'>

<div class="container-fluid">

    <h1>Bienvenue sur Hey Listen</h1>

    <div id="login-box">

        <h3>Connexion</h3>

        <c:if test="${not empty error}">
            <div class="has-error">${error}</div>
        </c:if>
        <c:if test="${not empty msg}">
            <div class="text-info">${msg}</div>
        </c:if>

        <form name='loginForm'
              action="<c:url value='j_spring_security_check' />" method='POST'>

            <div class="form-group">
                <label for="username">Utilisateur</label>
                <input type="text" id="username" name="username" value="" />
            </div>

            <div class="form-group">
                <label for="password">Utilisateur</label>
                <input type="password" id="password" name="password" value="" />
            </div>

            <div>
                <input  class="btn btn-primary" name="submit" type="submit"
                        value="Connexion"/>
            </div>

            <input type="hidden" name="${_csrf.parameterName}"
                   value="${_csrf.token}"/>

        </form>
    </div>

    <div>
        <a class="btn btn-default" href="<c:url value="/effetindesirable/all" />">Effets ind&eacute;sirables recens&eacute;s</a>
    </div>

</div>

</body>
<script src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script src="https://code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"/>

</html>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>Liste des effets ind&eacute;sirable dues &agrave; des produits cosm&eacute;tique</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
    <div class="row col-md-12">
        <p class="text-center text-info">Cette page contient l'ensemble des notifications d'effet ind&eacute;sirable pour des produits cosm&eacute;tique</p>
        <p class="text-center text-primary">${fn:length(notificationsProduits)} r&eacute;sultat(s)</p>
    </div>

    <table class="table row col-md-12">
        <thead>
        <tr>
            <th>Effet ind&eacute;sirable</th>
            <th>Produit cosm&eacute;tique</th>
            <th>Utilisateur li&eacute;</th>
            <th>Derni&egrave;re d&eacute;claration</th>
            <th>Action</th>
        </tr>
        </thead>
        <c:forEach items="${notificationsProduits}" var="notification">
            <tbody>
            <tr>
                <td><a href="<c:url value="/effetindesirable/detail?id=${notification.effetIndesirable.idEffetIndesirable}" />">${notification.effetIndesirable.libelle}</a></td>
                <td><a href="<c:url value="/produitCosmetique/detail?id=${notification.produit.idProduit}" />">${notification.produit.libelle}</a></td>
                <td>${notification.utilisateur.username}</td>
                <td>${notification.dateNotif}</td>
                <td>
                    <a href="<c:url value="/notification/deleteNotificationProduit?id=${notification.idNotifProduit}" />" class="btn btn-sm btn-danger">Supprimer</a>
                </td>
            </tr>
            </tbody>
        </c:forEach>
    </table>

    <div class="row col-md-12">
        <p><a href="<c:url value="/notification/creerNotificationProduitCosmetique"/>" class="btn btn-default">Notifier un effet ind&eacute;sirable</a></p>
    </div>
</div>
</body>
</html>
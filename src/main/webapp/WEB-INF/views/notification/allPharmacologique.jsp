<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>Cas d'effets ind&eacute;sirables connus</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
    <div class="row col-md-12">
        <p class="text-center text-info">Cette page contient l'ensemble des cas d'effet ind&eacute;sirable connus pour des classes pharmacologiques</p>
        <p class="text-center text-primary">${fn:length(casPharmacologiqueEI)} r&eacute;sultat(s)</p>
    </div>

    <table class="table row col-md-12">
        <thead>
        <tr>
            <th>Effet ind&eacute;sirable</th>
            <th>Classe pharmacologique concern&eacute;</th>
            <th>Action</th>
        </tr>
        </thead>
        <c:forEach items="${casPharmacologiqueEI}" var="cas">
            <tbody>
            <tr>
                <td><a href="<c:url value="/effetindesirable/detail?id=${cas.effetIndesirableClassePharmacologique.idEffetIndesirable}" />">${cas.effetIndesirableClassePharmacologique.libelle}</a></td>
                <td>${cas.classePharmacologique.libelle}</td>
                <td>
                    <a href="<c:url value="/effetindesirable/detail?id=${cas.effetIndesirableClassePharmacologique.idEffetIndesirable}" />" class="btn btn-sm btn-info">Consulter</a>
                </td>
            </tr>
            </tbody>
        </c:forEach>
    </table>

    <div class="row col-md-12">
        <p><a href="<c:url value="/effetindesirable/creer"/>" class="btn btn-default">Ajouter un effet ind&eacute;sirable</a></p>
    </div>
</div>
</body>
</html>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>Cr&eacute;tion d'une nouvelle notification</title>
</head>
<body>
<c:import url="../menu.jsp" />

<div class="container-fluid">
    <div class="row col-md-4">
        <form:form commandName="newNotificationDispositifMedical" action="ajouterNotificationDispositifMedical">
            <div class="form-group">
                <form:label path="effetIndesirable">Votre effet ind&eacute;sirable</form:label>
                <form:input path="effetIndesirable" cssClass="form-control" required="required" />
                <form:errors path="effetIndesirable" cssClass="has-error" />
            </div>

            <div class="form-group">
                <form:label path="dispositif">Le dispositif m&eacute;dical que vous avez utilis&eacute; </form:label>
                <form:select path="dispositif" cssClass="form-control" items="${dispositifs}" itemValue="idDispositif" multiple="false" />
                <form:errors path="dispositif" cssClass="has-error" />
            </div>

            <a href="<c:url value="/notification/allDispositif" />" class="btn btn-warning">Retour</a>
            <input type="submit" class="btn btn-primary" value="Ajouter" />
        </form:form>
    </div>
</div>
</body>
</html>
